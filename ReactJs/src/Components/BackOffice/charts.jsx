// ##############################
// // // Chart variables
// #############################

// chartExample1 and chartExample2 options

var d = new Date();

let year = d.getFullYear();

let month = d.getMonth();


let routes = [];
let matches = [];
let logins = [];
let users = [];
let llabel = [];
for(let i = 1;i<=31;i++)
  llabel.push(i);

fetch("https://tour-tuga.appspot.com/getstats?date="+year+""+month)
.then((response)=>{
  return response.json();
}).then(json=>{
  for(let i in json){
    routes[i] = json[i].routes;
    matches[i] = json[i].matches;
    logins[i] = json[i].logins;
    users[i] = json[i].users;
  }
}).catch(function(error) {
  console.log('There has been a problem with your fetch operation: ' + error.message);
});

let chart1_2_options = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  responsive: true,
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }
    ],
    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.1)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }
    ]
  }
};

// #########################################
// // // used inside src/views/Dashboard.jsx
// #########################################
let chartExample1 = {
  data1: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors
    return{
      labels: llabel,
      datasets: [
        {
          label: "Number of new routes",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: routes
        }
      ]
    };
  },
  data2: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors
    return {
      labels: llabel,
      datasets: [
        {
          label: "Number of new Users",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: users
        }
      ]
    };
  },
  data3: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors
    return {
      labels: llabel,
      datasets: [
        {
          label: "Number of new matches",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: matches
        }
      ]
    };
  },
  data4: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors
    return {
      labels: llabel,
      datasets: [
        {
          label: "Number of new logins",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: logins
        }
      ]
    };
  },
  options: chart1_2_options
};


// // #########################################
// // // // used inside src/views/Dashboard.jsx
// // #########################################
// let chartExample2 = {
//   data: canvas => {
//     let ctx = canvas.getContext("2d");

//     let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

//     gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
//     gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
//     gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

//     return {
//       labels: ["JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
//       datasets: [
//         {
//           label: "Data",
//           fill: true,
//           backgroundColor: gradientStroke,
//           borderColor: "#1f8ef1",
//           borderWidth: 2,
//           borderDash: [],
//           borderDashOffset: 0.0,
//           pointBackgroundColor: "#1f8ef1",
//           pointBorderColor: "rgba(255,255,255,0)",
//           pointHoverBackgroundColor: "#1f8ef1",
//           pointBorderWidth: 20,
//           pointHoverRadius: 4,
//           pointHoverBorderWidth: 15,
//           pointRadius: 4,
//           data: [80, 100, 70, 80, 120, 80]
//         }
//       ]
//     };
//   },
//   options: chart1_2_options
// };

// // #########################################
// // // // used inside src/views/Dashboard.jsx
// // #########################################
// let chartExample3 = {
//   data: canvas => {
//     let ctx = canvas.getContext("2d");

//     let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

//     gradientStroke.addColorStop(1, "rgba(72,72,176,0.1)");
//     gradientStroke.addColorStop(0.4, "rgba(72,72,176,0.0)");
//     gradientStroke.addColorStop(0, "rgba(119,52,169,0)"); //purple colors

//     return {
//       labels: ["USA", "GER", "AUS", "UK", "RO", "BR"],
//       datasets: [
//         {
//           label: "Countries",
//           fill: true,
//           backgroundColor: gradientStroke,
//           hoverBackgroundColor: gradientStroke,
//           borderColor: "#d048b6",
//           borderWidth: 2,
//           borderDash: [],
//           borderDashOffset: 0.0,
//           data: [53, 20, 10, 80, 100, 45]
//         }
//       ]
//     };
//   },
//   options: {
//     maintainAspectRatio: false,
//     legend: {
//       display: false
//     },
//     tooltips: {
//       backgroundColor: "#f5f5f5",
//       titleFontColor: "#333",
//       bodyFontColor: "#666",
//       bodySpacing: 4,
//       xPadding: 12,
//       mode: "nearest",
//       intersect: 0,
//       position: "nearest"
//     },
//     responsive: true,
//     scales: {
//       yAxes: [
//         {
//           gridLines: {
//             drawBorder: false,
//             color: "rgba(225,78,202,0.1)",
//             zeroLineColor: "transparent"
//           },
//           ticks: {
//             suggestedMin: 60,
//             suggestedMax: 120,
//             padding: 20,
//             fontColor: "#9e9e9e"
//           }
//         }
//       ],
//       xAxes: [
//         {
//           gridLines: {
//             drawBorder: false,
//             color: "rgba(225,78,202,0.1)",
//             zeroLineColor: "transparent"
//           },
//           ticks: {
//             padding: 20,
//             fontColor: "#9e9e9e"
//           }
//         }
//       ]
//     }
//   }
// };

// // #########################################
// // // // used inside src/views/Dashboard.jsx
// // #########################################
// const chartExample4 = {
//   data: canvas => {
//     let ctx = canvas.getContext("2d");

//     let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

//     gradientStroke.addColorStop(1, "rgba(66,134,121,0.15)");
//     gradientStroke.addColorStop(0.4, "rgba(66,134,121,0.0)"); //green colors
//     gradientStroke.addColorStop(0, "rgba(66,134,121,0)"); //green colors

//     return {
//       labels: ["JUL", "AUG", "SEP", "OCT", "NOV"],
//       datasets: [
//         {
//           label: "My First dataset",
//           fill: true,
//           backgroundColor: gradientStroke,
//           borderColor: "#00d6b4",
//           borderWidth: 2,
//           borderDash: [],
//           borderDashOffset: 0.0,
//           pointBackgroundColor: "#00d6b4",
//           pointBorderColor: "rgba(255,255,255,0)",
//           pointHoverBackgroundColor: "#00d6b4",
//           pointBorderWidth: 20,
//           pointHoverRadius: 4,
//           pointHoverBorderWidth: 15,
//           pointRadius: 4,
//           data: [90, 27, 60, 12, 80]
//         }
//       ]
//     };
//   },
//   options: {
//     maintainAspectRatio: false,
//     legend: {
//       display: false
//     },

//     tooltips: {
//       backgroundColor: "#f5f5f5",
//       titleFontColor: "#333",
//       bodyFontColor: "#666",
//       bodySpacing: 4,
//       xPadding: 12,
//       mode: "nearest",
//       intersect: 0,
//       position: "nearest"
//     },
//     responsive: true,
//     scales: {
//       yAxes: [
//         {
//           barPercentage: 1.6,
//           gridLines: {
//             drawBorder: false,
//             color: "rgba(29,140,248,0.0)",
//             zeroLineColor: "transparent"
//           },
//           ticks: {
//             suggestedMin: 50,
//             suggestedMax: 125,
//             padding: 20,
//             fontColor: "#9e9e9e"
//           }
//         }
//       ],

//       xAxes: [
//         {
//           barPercentage: 1.6,
//           gridLines: {
//             drawBorder: false,
//             color: "rgba(0,242,195,0.1)",
//             zeroLineColor: "transparent"
//           },
//           ticks: {
//             padding: 20,
//             fontColor: "#9e9e9e"
//           }
//         }
//       ]
//     }
//   }
// };

module.exports = {
  chartExample1 // in src/views/Dashboard.jsx
  // chartExample2, // in src/views/Dashboard.jsx
  // chartExample3, // in src/views/Dashboard.jsx
  // chartExample4 // in src/views/Dashboard.jsx
};

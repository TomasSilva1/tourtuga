import React,{Component} from 'react';
import { Login } from './Login.js';
import $ from 'jquery';
import avatar from '../../images/avatar.png';

class DestinationsPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            login:false,
            ref:'#Destinations',
            routeClicked:false,
            photo:avatar,
            newMessage:false
        }
        this.showLogin=this.showLogin.bind(this);
        this.handleRoute=this.handleRoute.bind(this);
        this.handleLogOut =this.handleLogOut.bind(this);
    }

    showLogin(){
        this.setState({login:true});
        document.querySelector("#login").style.display = 'block';
    }

    handleRoute(){
        if(sessionStorage.getItem('tokenID')!==null){
            this.setState({ref:'#CreateRoutes'});
        }else{
            alert("You need to be logged in.");
            sessionStorage.setItem('routeClicked',true);
        }
    }

    componentDidMount(){
        if(sessionStorage.getItem("tokenID")){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({tokenID:tokenID, email:email}),
                success: (data)=>{
                    var photo = data.photo;
                    if(!data.photo){
                        photo = avatar;
                    }
                    this.setState({photo:photo,newMessage:data.newMessage});
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    console.log(data);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    

                    }
                },
            })
        }
    }
      

    handleLogOut(e){
        e.preventDefault();
        $.ajax({
            type: "DELETE",
            url: "https://tour-tuga.appspot.com/logout",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            data:JSON.stringify({
                tokenID:sessionStorage.getItem('tokenID'),
                email:sessionStorage.getItem('email')}
            ),
            success: ()=> {
                    var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                    alert("See you later " + fullName)
                    sessionStorage.clear();
                    window.location.hash = "/";
            },
            error: function (data) {
                alert("Error: " + data.status);
                if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                           window.location.hash = '/';                    

                }
            },
        })
    }

    
    render(){
        const login = this.state.login;
        const tokenID = sessionStorage.getItem('tokenID');
        const newMessage = this.state.newMessage;
        return(
            <div className="DestinationsPage">
                <div className="firstpage-top">
                <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    {tokenID === null &&
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <a href="#Register" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Sign up</a>
                        <a href="#Destinations" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button login"
                            onClick={this.showLogin}
                            data-flow-name="core_combined">Login</a>
                    </div>
                    }
                    {tokenID !== null && 
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations"  className="firstpage-bar-item firstpage-button ">Destinations</a>
                            <div className="dropdown">
                                <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                    onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                                </button>
                                <div className="dropdown-content" id="myDropdown">
                                    <a href="#Profile">Profile</a>
                                    <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                                </div>
                            
                                {!newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                            </div>
                            }
                            {newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                            </div>
                            }</div>
                    </div>
                    }
                </div>
                </div>

                <div className="w3-main w3-light-grey" style={{padding:"10%",paddingBottom:"220px"}}>

                    <div className="w3-row-padding w3-margin-bottom">
                        <div className="w3-quarter">
                            <div className="w3-container w3-padding-16">
                                <div className="w3-clear"></div>
                            </div>
                        </div>
                        <div className="w3-quarter">
                            <a href="#RecomDestinations">
                                <div className="w3-container w3-green w3-padding-16">
                                    <div className="w3-left">
                                        <i class="fa fa-globe w3-xxxlarge"></i>
                                    </div>
                                    <div className="w3-clear"></div>
                                    <h4>Recomendations</h4>
                                </div>
                            </a>
                        </div>
                        <div className="w3-quarter">
                            <a href={this.state.ref} onClick={this.handleRoute} >
                                <div className="w3-container w3-blue w3-padding-16">
                                    <div className="w3-left">
                                        <i className="fa fa-user-circle w3-xxxlarge"></i>
                                    </div>
                                    <div class="w3-right"></div>
                                    <div class="w3-clear"></div>
                                    <h4>Personalize new route</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="login" className="modal">
                    {login &&
                        <Login/>
                    }
                </div>
                
                <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                            style={{display:"none"}} id="mySidebar">
                            <a href="#About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                            <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                            <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact us</a>
                            <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                            <a href="#Register" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Sign up</a>
                            <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button login"
                                onClick={this.showLogin} data-flow-name="core_combined">Login</a>
                        </nav>

                        <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%",paddingTop:"1%"}}>
                            <a href="#about" id="ToTheTopButton"  className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>
                

            </div>
        )
    }


}

export default DestinationsPage;
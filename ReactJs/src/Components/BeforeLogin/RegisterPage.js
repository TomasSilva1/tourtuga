import React,{Component} from 'react';
import ReactFlagsSelect from 'react-flags-select';
import { Login } from './Login.js';
import './css/registerpage.css';
import 'react-flags-select/css/react-flags-select.css';
import $ from 'jquery';

//TALVEZ EXTENDER CLASSE LOGIN E TIRAR OS HREF VOIDS
class RegisterPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            login:false,
            firstname:'',
            lastname:'',
            email:'',
            age:0,
            telemovel:'',
            country:'',
            password:'',
            privado:'public'
        }
        this.showLogin = this.showLogin.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSelectFlag = this.handleSelectFlag.bind(this);
        this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
    }

    showLogin(){
        this.setState({login:true});
        document.querySelector("#login").style.display = 'block';
    }

    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
    }

    handleSelectFlag(countryCode){
        this.setState({country:countryCode});
    }

    handleRegisterSubmit(e){
        e.preventDefault();
            $.ajax({
                type: "POST",
                url: "https://tour-tuga.appspot.com/registUser",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    firstname:this.state.firstname,
                    lastname:this.state.lastname,
                    email : this.state.email,
                    photo:'',
                    telemovel:this.state.telemovel,
                    country:this.state.country,
                    password : this.state.password,
                    age:this.state.age,
                    privado:this.state.privado
                }),
                success: (data)=>{
                    if(data.error)
                        alert("User already exist");
                    else{
                        alert("got token with id: " + data.tokenID);
                        alert("User registered: " + this.state.firstname + " " + this.state.lastname);
                        sessionStorage.setItem('email', this.state.email);
                        sessionStorage.setItem('firstname', this.state.firstname);
                        sessionStorage.setItem('lastname', this.state.lastname);
                        sessionStorage.setItem('tokenID',data.tokenID);
                        sessionStorage.setItem('newMessage',data.newMessage);
                        window.location.hash = '/FavTourism';
                    }
                },
                error:(data)=> {
                    alert("Error: " + data.status)
                },
            })
        }

    render(){
        const login = this.state.login;
        return(
        <div className = "RegisterPage">
                <div className="firstpage-top">
                <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <a href="#Register" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Sign up</a>
                        <a href="#Register" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button login"
                            onClick={this.showLogin}
                            data-flow-name="core_combined">Login</a>
                    </div>
                </div>
        </div>
            
        
            <div id="register" className = "firstpage-light-grey">
                <form name="registerRequest">
                    <div className="container">
                        <section className ="register cf">
                            <h1>Create an account</h1>
                            <hr/>
                            <label htmlFor="firstname">FirstName</label>
                            <label htmlFor="lastname" style={{marginLeft:"44%"}}>LastName</label>
                            <br/>
                            <input type="text" id="RFN" name="firstname" onChange ={this.handleTextChange} required/>
                            <input type="text" id ="RLN" name="lastname" onChange ={this.handleTextChange} required/>
                            <br/>
                            <label htmlFor="email">Email</label>
                            <label htmlFor="telemovel" style={{marginLeft:"47%"}}>Telemovel</label>
                            <br/>
                            <input id="Remail" type="email" name="email" onChange ={this.handleTextChange} placeholder="example@gmail.com" required/>
                            <input id="Rtelemovel" type="tel" name="telemovel" onChange ={this.handleTextChange} pattern="[0-9]{3}-[0-9]{3}-[0-9]{3}" required/>
                            <br/>
                            <label htmlFor="password">Password</label>
                            <input id="password" type="password" name="password" placeholder="password" onChange ={this.handleTextChange} required/>
                            
                            <label htmlFor ="country">Country</label>
                            <div className="menu-flags">
                                <ReactFlagsSelect className="menu-flags" alignOptions="left" optionsSize={15} 
                                selectedSize={25} searchable={true} searchPlaceholder="Search for a country"
                                onSelect={this.handleSelectFlag} showSelectedLabel={false} defaultCountry="US" />
                            </div>
                            <br/>
                            <br/>
                            <label htmlFor="age">Age</label>
                            <br/>
                            <input name ="age" type="number"  min="16" max="99" onChange={this.handleTextChange} required ></input>
                            <br/>
                            <label htmlFor="privado">Profile</label>
                            <br/>
                            <select id="RGprofile" name="privado" placeholder="Profile" onChange ={this.handleTextChange} >
                                <option value="private">Privado</option>
                                <option value="public" selected>Publico</option>
                            </select>
                            <br/>
                            <label>
                                <input type="checkbox" name="remember"/> Remember me
                            </label>
                            <br/>
                            <button onClick={this.handleRegisterSubmit} type="submit">Create account</button>
                            <span className="cna" id="haveA" onClick={this.showLogin}><a
                                    href="#Register">Already have an account?</a></span>
                            <br/>
                        </section>
                    </div>
                </form>
            </div>

            <div id="login" className="modal">
            {login && 
                      <Login/>
                }
            </div>
                    
        
            <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                style={{display:"none"}} id="mySidebar">
                <a href="About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                <a href="ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact us</a>
                <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                <a href="#signup" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Sign up</a>
                <a href="#Register" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button login"
                    onClick={this.showLogin} data-flow-name="core_combined">Login</a>
            </nav>
        
        
            <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%"}}>
                            <a href="#about" id="ToTheTopButton" className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>
        
        </div>
        

        )

    }








}

export default RegisterPage;
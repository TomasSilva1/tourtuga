import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { Login } from './Login.js';
import '../BeforeLogin/css/firstpage.css';
import avatar from '../../images/avatar.png';
import $ from 'jquery';
import firstLE from '../../images/firstPageLE/iStock-545347988.jpg';
import secondLE from '../../images/firstPageLE/Street-View-of-Houses-in-Pennsylvania.jpg';
import thirdLE from '../../images/firstPageLE/tourists-in-lisbon.jpg';
import { Slide } from 'react-slideshow-image';


//METER O SLIDE A FUNCIOANR
class FirstPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            login:false,
            slideIndex:1,
            newMessage:false,
        }
        this.showLogin = this.showLogin.bind(this);
        this.handlePhoto = this.handlePhoto.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
    }

    //MUDAR ISTO PARA COMO ESTA NO CHATBOX
    componentDidMount(){
        if(sessionStorage.getItem('tokenID'))
            this.handlePhoto();
        const div= document.getElementById("slideShow");
        const slideImages = [
            firstLE,
            secondLE,
            thirdLE
        ];
        const properties = {
            duration: 5000,
            transitionDuration: 500,
            infinite: true,
            indicators: true,
            arrows: true
        }
        const element=(
            <Slide {...properties}>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[0]})`}}>
                        <span>Slide 1</span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[1]})`}}>
                        <span>Slide 2</span>
                    </div>
                </div>
                <div className="each-slide">
                    <div style={{'backgroundImage': `url(${slideImages[2]})`}}>
                        <span>Slide 3</span>
                    </div>
                </div>
            </Slide>
        );
        ReactDOM.render(element,div);
    }
    
    
    showLogin(){
        this.setState({login:true});
        document.querySelector("#login").style.display = 'block';
    }

    handlePhoto(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({tokenID:tokenID, email:email}),
                success: (data)=>{
                    var photo = data.photo;
                    if(!data.photo){
                        photo=avatar;
                    }
                    this.setState({photo:photo,newMessage:data.newMessage});
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    console.log(data);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    
                    }
                },
            })
        }

        

        handleLogOut(e){
            e.preventDefault();
            $.ajax({
                type: "DELETE",
                url: "https://tour-tuga.appspot.com/logout",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                data:JSON.stringify({
                    tokenID:sessionStorage.getItem('tokenID'),
                    email:sessionStorage.getItem('email')}
                ),
                success: ()=> {
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("See you later " + fullName)
                        sessionStorage.clear();
                        window.location.reload(true);
                },
                error: function (data) {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.reload(true);                    
                    }
                },
            })
        }

    render() {
        const login = this.state.login;
        const tokenID = sessionStorage.getItem("tokenID");
        const newMessage = this.state.newMessage;
        return (
            <div className = "FirstPage">
                <div className="firstpage-top">
                <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    {tokenID === null &&
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <a href="#Register" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Sign up</a>
                        <a href="/#" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button login"
                            onClick={this.showLogin}
                            data-flow-name="core_combined">Login</a>
                    </div>
                    }
                    {tokenID !== null && 
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations"  className="firstpage-bar-item firstpage-button ">Destinations</a>
                            <div className="dropdown">
                                <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                    onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                                </button>
                                <div className="dropdown-content" id="myDropdown">
                                    <a href="#Profile">Profile</a>
                                    <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                                </div>
                            </div>
                            {!newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                            </div>
                            }
                            {newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                            </div>
                            }
                    </div>
                    }
                </div>
                </div>
            
            <div className="firstpage-light-grey" id="slideShow" style={{paddingTop:"10%",paddingBottom:"10%"}}></div>
            
            <div id="login" className="modal">
            {login &&
                <Login/>
            }
            </div>
                                
            <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                style={{display:"none"}} id="mySidebar">
                <a href="About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                <a href="ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact us</a>
                <a href="Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                <a href="#signup" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Sign up</a>
                <a href="/#" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button login"
                    onClick={this.showLogin} data-flow-name="core_combined">Login</a>
            </nav>
        
        
            <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%"}}>
                            <a href="#about" id="ToTheTopButton" className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>
        </div>
        
                
        )
    }
}
export default FirstPage;
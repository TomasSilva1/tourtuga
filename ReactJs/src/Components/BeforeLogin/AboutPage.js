import React,{Component} from 'react';
import { Login } from './Login.js';
import '../BeforeLogin/css/contact.css';
import avatar from '../../images/avatar.png';
import bruno from '../../images/Team/bruno.jpeg';
import filipe from '../../images/Team/filipe.jpeg';
import fred from '../../images/Team/fred.jpeg';
import tomas from '../../images/Team/tomas.jpeg';
import mario from '../../images/Team/mario.jpeg';
import $ from 'jquery';


class AboutPage extends Component{
        constructor(props){
            super(props);
            this.state ={
                login:false,
                photo:avatar,
                newMessage:false
            }
            this.showLogin = this.showLogin.bind(this);
            this.handleLogOut = this.handleLogOut.bind(this);        
        }
        
        showLogin(){
            this.setState({login:true});
            document.querySelector("#login").style.display = 'block';
        }

        componentDidMount(){
            if(sessionStorage.getItem("tokenID")){
            var tokenID = sessionStorage.getItem("tokenID");
            var email = sessionStorage.getItem("email");
                $.ajax({
                    type: "PUT",
                    url: "https://tour-tuga.appspot.com/getuserinfo",
                    contentType: "application/json; charset=utf-8",
                    crossDomain: true,
                    dataType: "json",
                    data : JSON.stringify({tokenID:tokenID, email:email}),
                    success: (data)=>{
                        var photo = data.photo;
                        if(!data.photo){
                            photo = avatar;
                        }
                        this.setState({photo:photo,newMessage:data.newMessage});
                    },
                    error:(data)=> {
                        alert("Error: " + data.status);
                        console.log(data);
                        if(data.responseJSON.Error === 'InvalidToken'){
                            sessionStorage.clear();
                            window.location.hash = '/';                    
    
                        }
                    },
                })
            }
        }
            
        handleLogOut(e){
            e.preventDefault();
            $.ajax({
                type: "DELETE",
                url: "https://tour-tuga.appspot.com/logout",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                data:JSON.stringify({
                    tokenID:sessionStorage.getItem('tokenID'),
                    email:sessionStorage.getItem('email')}
                ),
                success: ()=> {
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("See you later " + fullName)
                        sessionStorage.clear();
                        window.location.hash = "/";
                },
                error: function (data) {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                               window.location.hash = '/';                    

                    }
                },
            })
        }

        render(){
            const login = this.state.login;
            const tokenID = sessionStorage.getItem('tokenID');
            const newMessage = this.state.newMessage;
            return(
                <div className = "About">
                        <div className="firstpage-top" id="about" >
                        <div className="firstpage-white firstpage-card" id="myNavbar">
                        <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                        </a>
                    {tokenID === null &&
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <a href="#Register" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button">Sign up</a>
                        <a href="#ContactUs" style={{marginTop:"2%"}} className="firstpage-bar-item firstpage-button login"
                            onClick={this.showLogin}
                            data-flow-name="core_combined">Login</a>
                    </div>
                    }
                    {tokenID !== null && 
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About" className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations"  className="firstpage-bar-item firstpage-button ">Destinations</a>
                            <div className="dropdown">
                                <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                    onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                                </button>
                                <div className="dropdown-content" id="myDropdown">
                                    <a href="#Profile">Profile</a>
                                    <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                                </div>
                            </div>
                            {!newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                            </div>
                            }
                            {newMessage &&
                            <div className="firstpage-right">
                                <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                            </div>
                            }
                    </div>
                    }
                
                    </div>
                    </div>


                        <div className="firstpage-container firstpage-light-grey">  
                            <div className="w3-container" style={{padding:"128px 16px",marginLeft:"3%"}} id="about">
                                <h3 className="w3-center">ABOUT TOURTUGA</h3>
                                <p className="w3-center w3-large">Key features of our app</p>
                                <div className="w3-row-padding w3-center" style={{marginTop:"64px"}}>
                                    <div className="w3-quarter">
                                        <i className="fa fa-desktop w3-margin-bottom w3-jumbo w3-center"></i>
                                        <p className="w3-large">Usability</p>
                                        <p>Easy to use, easy to travel!</p>
                                    </div>
                                    <div className="w3-quarter">
                                        <i className="fa fa-heart w3-margin-bottom w3-jumbo"></i>
                                        <p className="w3-large">Cozy</p>
                                        <p>Connect with people. Don't be a lonely Joe!</p>
                                    </div>
                                    <div className="w3-quarter">
                                        <i className="fa fa-diamond w3-margin-bottom w3-jumbo"></i>
                                        <p className="w3-large">Design</p>
                                        <p>Intuitive, fun and interative.</p>
                                    </div>
                                    <div className="w3-quarter">
                                        <i className="fa fa-cog w3-margin-bottom w3-jumbo"></i>
                                        <p className="w3-large">Support</p>
                                        <p>If you need our help, just send us an email through the <a href="#ContactUs">Contact Us</a> link.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="w3-container" id="team"></div>
                            <h3 className="w3-center">CLOUDFIVE</h3>
                            <p className="w3-center w3-large">The ones who runs this teams</p>
                            <div className="w3-row-padding w3-grayscale" style={{marginTop:"64px"}}>
                                <div className="w3-col l3 m6 w3-margin-bottom">
                                    <div className="w3-card">
                                        <img src={bruno} alt="Bruno"/>
                                        <div className="w3-container">
                                            <h3>Bruno Santos</h3>
                                            <p className="w3-opacity">BackEnd</p>
                                            <p>Cenas</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="w3-col l3 m6 w3-margin-bottom">
                                    <div className="w3-card">
                                        <img src={filipe} alt="Filipe"/>
                                        <div className="w3-container">
                                            <h3>Filipe Santos</h3>
                                            <p className="w3-opacity">BackEnd</p>
                                            <p>Cenas</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="w3-col l3 m6 w3-margin-bottom">
                                    <div className="w3-card">
                                        <img src={fred} alt="Fred"/>
                                        <div className="w3-container">
                                            <h3>Frederico Lopes</h3>
                                            <p className="w3-opacity">FrontEnd</p>
                                            <p>Cenas</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="w3-col l3 m6 w3-margin-bottom">
                                    <div className="w3-card">
                                        <img src={mario} alt="Mario"/>
                                        <div className="w3-container">
                                            <h3>Mário Bello</h3>
                                            <p className="w3-opacity">FrontEnd</p>
                                            <p>Cenas</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="w3-col l3 m6 w3-margin-bottom ">
                                    <div className="w3-card">
                                        <img src={tomas} alt="tomas"/>
                                        <div className="w3-container">
                                            <h3>Tomás Silva</h3>
                                            <p className="w3-opacity">Android</p>
                                            <p>Cenas</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="login" className="modal">
                            {login && 
                                <Login/>
                            }
                        </div>


                        <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                            style={{display:"none"}} id="mySidebar">
                            <a href="#AboutPage" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                            <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                            <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact us</a>
                            <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                            <a href="#Register" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Sign up</a>
                            <a href="#AboutPage" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button login"
                                onClick={this.showLogin} data-flow-name="core_combined">Login</a>
                        </nav>

                        <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%"}}>
                            <a href="#about" id="ToTheTopButton"  className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>
                </div>
                )


        }
}

export default AboutPage;
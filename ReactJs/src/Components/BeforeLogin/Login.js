import React,{Component} from 'react';
import avatar from '../../images/avatar.png';
import $ from 'jquery';
import firebase from 'firebase';
import { pathToFileURL } from 'url';

export class Login extends Component{
    constructor(props){
        super(props);
        this.state ={
            login:false,
            email:'',
            password:'',
            newMessage:false
        }
        this.hideLogin = this.hideLogin.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        
    }

    hideLogin(){
        document.querySelector("#login").style.display = 'none';
    }

    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
    }
    

    handleLoginSubmit(e){
        sessionStorage.clear();
        var t;
        const messaging = firebase.messaging();
        messaging.requestPermission().then(function () {
            console.log('Notification permission granted.');
            return messaging.getToken();
        }).then( (token)=> {
            t = token;
            console.log(token);
            fetch("https://tour-tuga.appspot.com/devicetoken?email="+this.state.email+"&token="+token,{
                method:'PUT',
            }).then();
        }).catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });
        messaging.onMessage(function (payload) {
            console.log("onMessage");
            console.log(payload);
            alert(payload.notification.title + ": " + payload.notification.body);
        });
        e.preventDefault();   
        $.ajax({
            type: "POST",
            url: "https://tour-tuga.appspot.com/login",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            dataType: "json",
            data : JSON.stringify({
                email : this.state.email,
                password : this.state.password,
                photo:false,
                deviceToken:t
            }),
            success: (data)=>{
                sessionStorage.setItem('tokenID', data.tokenID);
                sessionStorage.setItem('email', this.state.email);
                sessionStorage.setItem('firstname', data.firstname);
                sessionStorage.setItem('lastname', data.lastname);
                sessionStorage.setItem('role', data.role);
                var fullName = data.firstname+" "+data.lastname;
                alert("Welcome: " + fullName);
                if(sessionStorage.getItem('routeClicked')){
                    sessionStorage.removeItem('routeClicked');
                    window.location.hash = '/CreateRoutes';
                }else{
                    if(data.role == 1)
                        window.location.hash = '/BackOffice';
                    else
                        window.location.hash = '/Profile';
                }
            },
            error:(data)=> {
                alert("Error: " + data.status);
                if(data.pwerror)
                    alert("Wrong password!");
                else if(data.error)
                    alert("User doesn't exist");
            },
        })
    }
    
render(){
    return(    
        <form name="loginRequest" id="LGcontainer" className="modal-content animate" onSubmit = {this.handleLoginSubmit}>
            <div className="imgcontainer">
                <span onClick={this.hideLogin} className="close"
                title="Close Modal">&times;</span>
                <img src={avatar} alt="Avatar" className="avatar"/>
            </div>
            <div className="container" id="LGsection">
                <section className="loginform cf">
                    <legend>Login</legend>
                    <label htmlFor="email">Email</label> 
                    <input type="email" name="email" placeholder="example@gmail.com" onChange ={this.handleTextChange} required/> 
                        <label htmlFor="password">Password</label> 
                        <input type="password" name="password"
                        placeholder="password" onChange = {this.handleTextChange} required/>
                    <label>
                        <input type="checkbox" name="remember"/> Remember me
                    </label>
                    <br/>
                    <button onClick ={this.handleLoginSubmit}>Login</button>
                    <br/>
                    {/* METER FORGOT PASSWORD A FUNCIONAR*/}
                    <span className="psw"><a href="#forgotpass">Forgot password?</a></span>
                    <span className="cna"><a href="#Register">Create new Account?</a></span>
                </section>
            </div>
         </form>         
    );
    
}
    
    
}


export default Login;
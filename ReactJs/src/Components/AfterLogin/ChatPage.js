import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import '../AfterLogin/css/profilepage.css';
import avatar from '../../images/avatar.png';
import $ from 'jquery';


class ChatPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            firstname:'',
            lastname:'',
            photo: avatar,
            matchedEmail:'',
            matchedName:'',
            matchedPhotos:[],
            matchAllChat:[],//conversa inteira
            matches:[], //barra lateral
            newMessage:false,
            play:false
        }
        
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleCurrentConversation = this.handleCurrentConversation.bind(this);
        this.handleCurrentMatch = this.handleCurrentMatch.bind(this);
        this.handleBlockMatch = this.handleBlockMatch.bind(this);
        this.handleKeyPressed = this.handleKeyPressed.bind(this);
        this.handleSendMsg = this.handleSendMsg.bind(this);
        this.handleMsgFromChat = this.handleMsgFromChat.bind(this);
        this.getUserProfile = this.getUserProfile.bind(this);
        this.updateBlock = this.updateBlock.bind(this);
        this.handlegetUserConvs=this.handlegetUserConvs.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
        
    }

    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
    }

    //FAZER METODO QUE D UPDATE NA ULTIMA MSG NO CHAT E NO CHATBLOCK
    handleCurrentMatch(e){
        var matchedEmail=this.state.matchedEmail;
        if(e){
            matchedEmail = this.state.matches[e.target.id].dest;
            const matchedName = this.state.matches[e.target.id].name;
            this.setState({matchedEmail:matchedEmail,matchedName:matchedName});
        }
        this.handleMsgFromChat(matchedEmail);
    }

    //TALVEZ FAZER O MESMO QUE NO ADDCHAT
    handleCurrentConversation(matchedEmail){
        const div = document.getElementById("currentConversation");
        var matchedPhoto = avatar;
        if(this.state.matchedPhotos.find(x => x.email === matchedEmail)){
            matchedPhoto = this.state.matchedPhotos.find(x => x.email === matchedEmail).photo;
        }
        var blocks =[];
        const msgArea = (
            <div>
                <div style={{width:"75%",marginLeft:"8%",overflowY:"auto"}}>
                    <textarea className="firstpage-input firstpage-border" id="msgFromChat" type="text" placeholder="Type a msg..."
                        onChange = {this.handleTextChange} onKeyPress = {this.handleKeyPressed} required/>        
                </div>
                <div>
                    <a href="#ChatPage" onClick={()=>this.handleBlockMatch(matchedEmail)} 
                    style={{float:"right",fontSize:"36px",marginTop:"-6.5%",marginRight:"13%",color:"black"}}>
                        <i className="fa fa-ban"></i>
                    </a>        
                </div>
            </div>
        );
        for(var i =this.state.matchAllChat.length-1; i >=0; i--){
            const conversation = this.state.matchAllChat[i];
            var msg = conversation.msg;
            var time = conversation.hour+":"+conversation.minute;
            if(conversation.user === sessionStorage.getItem('email')){
                blocks.push(<div className="containerMsg" style={{width:"80%",marginLeft:"8%"}}>
                                <img src={this.state.photo} alt="Avatar"style={{width:"100%"}}/>
                                <p>{msg}</p>
                                <span className="time-right">{time}</span>
                            </div>);
            }else{
                blocks.push(<div className="containerMsg darker" style={{width:"80%", marginLeft:"8%"}}>
                                <img src={matchedPhoto} alt="Avatar" className="right" style={{width:"100%"}}/>
                                <p>{msg}</p>
                                <span className="time-left">{time}</span>
                            </div>);
            }
        }
        blocks.push(msgArea);
        ReactDOM.render(blocks,div);
    }

    handleKeyPressed(e){
        if(e.key === "Enter"){
            this.handleSendMsg();
            e.target.value="";
        }
    }

    handleBlockMatch(matchedEmail){
        var tokenID = sessionStorage.getItem('tokenID');
        var email = sessionStorage.getItem('email');
        $.ajax({
            type: "DELETE",
            url: "https://tour-tuga.appspot.com/blockmatch",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            dataType: "json",
            data : JSON.stringify({
                tokenID:tokenID, 
                email1:email,
                email2:matchedEmail,
            }),
            success: (data)=>{
                if(data.newMessage){
                    this.setState({newMessage:data.newMessage});
                }
                alert("Unmatched success");
                window.location.reload(true);
            },
            error:(data)=> {
                alert("Error: " + data.status);
                if(data.status === 0){
                    //sessionStorage.clear();
                    //window.location.hash = '/';                    
                }else if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                    window.location.hash = '/';
                }
            },
        })
    
    }

    updateBlock(msg,email){
        for(var i=0; i<this.state.matches.length;i++){
            if(this.state.matches[i].dest === this.state.matchedEmail){
                var final ={"msg":msg,
                    "dest":this.state.matchedEmail,
                    "name": this.state.matchedName,
                    "user": email,
                };
                var tmp = [...this.state.matches];
                tmp[i] = final;
                this.setState({matches:tmp});
                console.log(this.state.matches);
                break;
            }
        }
    }

    handleSendMsg(){
        var tokenID = sessionStorage.getItem('tokenID');
        var email = sessionStorage.getItem('email');
        var msg = document.getElementById("msgFromChat").value;
        if(msg){
            $.ajax({
                type: "POST",
                url: "https://tour-tuga.appspot.com/newMsg",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    tokenID:tokenID,
                    email:email,
                    destEmail:this.state.matchedEmail,
                    msg:msg
                }),
                success: (data)=>{
                    if(data.newMessage){
                        this.setState({newMessage:data.newMessage});
                    }
                    this.handleMsgFromChat(this.state.matchedEmail);
                    this.updateBlock(msg,email);
                    $( "#here" ).load(window.location.href + " #here" ); 
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                            window.location.hash = '/';                    

                    }
                },
            })
        }

    }

    handleMsgFromChat(matchedEmail){
        var tokenID = sessionStorage.getItem('tokenID');
        var email = sessionStorage.getItem('email');
        $.ajax({
            type: "PUT",
            url: "https://tour-tuga.appspot.com/getchatmessages",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            dataType: "json",
            data : JSON.stringify({
                tokenID:tokenID, 
                email:email,
                destEmail:matchedEmail,
                offset:""
            }),
            success: (data)=>{
                if(data.newMessage){
                    this.setState({newMessage:data.newMessage});
                }
                console.log(data.msgArray);
                //guardar offset
                const matchedName = this.state.matches.find(x => x.dest === matchedEmail).name;
                this.setState({matchAllChat:data.msgArray,matchedEmail:matchedEmail,matchedName:matchedName});
                this.handleCurrentConversation(matchedEmail);
            },
            error:(data)=> {
                alert("Error: " + data.status);
                if(data.status === 0){
                    //sessionStorage.clear();
                    //window.location.hash = '/';                    
                }else if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                    window.location.hash = '/';
                }
            },
        })
    
    }

    async getUserProfile(matchedEmail){
        const option = {method:"GET"};
        const url = "https://tour-tuga.appspot.com/getuserprofile?email="+matchedEmail
        const request = new Request(url,option);
        await fetch(request).then(response => response.json())
        .then(data => {
            if(data.photo)            
                this.setState({matchedPhotos:[...this.state.matchedPhotos,{photo:data.photo,email:matchedEmail}]});
            else{
                this.setState({matchedPhotos:[...this.state.matchedPhotos,{photo:avatar,email:matchedEmail}]});
            }
        });
        
    }

     handlegetUserConvs(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserconvs",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    tokenID:tokenID, 
                    email:email 
                }),
                success: (data)=>{
                    if(data.newMessage){
                        this.setState({newMessage:data.newMessage});
                    }
                    if(data.conversations.length>0){
                        this.setState({
                            matches:data.conversations
                        });
                        for(var i=0;i < this.state.matches.length;i++){
                            this.getUserProfile(this.state.matches[i].dest);
                        }
                        this.handleMsgFromChat(this.state.matches[0].dest);
                    }else{
                        alert("You have no matches yet!");
                        window.location.hash = '/Profile';
                    }
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    
                    }
                },
            })
    }

    componentDidMount(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    tokenID:tokenID, 
                    email:email
                }),
                success: (data)=>{
                    //alert(data.newMessage);
                    if(data.newMessage){
                        this.setState({newMessage:data.newMessage});
                        alert(data.newMessage);
                    }
                    var photo;
                    if(data.photo){
                        photo = data.photo;
                    }else{
                        photo = '../../images/avatar.png';
                    }
                    this.setState({
                        firstname :data.firstname,
                        lastname :data.lastname,
                        photo:photo
                    });
                    this.handlegetUserConvs();
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    

                    }
                },
            })
    }

    handleLogOut(e){
        e.preventDefault();
        $.ajax({
            type: "DELETE",
            url: "https://tour-tuga.appspot.com/logout",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            data:JSON.stringify({
                tokenID:sessionStorage.getItem('tokenID'),
                email:sessionStorage.getItem('email')}
            ),
            success: ()=> {
                    var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                    alert("See you later " + fullName)
                    sessionStorage.clear();
                    window.location.hash = "/";
            },
            error: (data) => {
                alert("Error: " + data.status);
                if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                    window.location.hash = '/';                    
                }
            },
        })
    }
    
    render(){
        //METER O BLOCK DE MANEIRA DIFERENTE TIPO COMO FACE
        const newMessage = this.state.newMessage;
        return( 
        <div className="ChatPage">
            <div className="firstpage-top ">
            <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About"  className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <div className="dropdown">
                            <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                            </button>
                            <div className="dropdown-content" id="myDropdown">
                                <a href="#Profile">Profile</a>
                                <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="firstpage-light-grey" id="profile" style={{paddingBottom:"20px"}} >
            <nav className="side-navbar firstpage-light-grey" id="PPsideBar" style={{marginLeft:"9%"}}>
                    <div className="sidebar-header d-flex align-items-center">
                        <div>
                        <img src={this.state.photo} alt="profileAvatar" id="PSPprofileAvatar" className="img-fluid rounded-circle"/>
                        </div>
                    </div>
                    <div className="title">
                            <h1 className="h4" >{this.state.firstname} {this.state.lastname}</h1>
                        </div>
                    <ul className="list-unstyled" id="CPnav">
                        {this.state.matches.map((item,i)=>{
                            const imgStyle = {height:'50px',width:'50px',float:"left",marginTop:"8px",display:"inline"};
                            var photo = avatar; 
                            if(this.state.matchedPhotos.find(x => x.email === item.dest)){
                                photo = this.state.matchedPhotos.find(x => x.email === item.dest).photo;  
                            }
                            let user;
                            if(item.user === sessionStorage.getItem('email')){
                                user = "You: ";
                            }else{
                                user = item.name+": ";
                            }
                            return (
                                    <li style={{overflow:"hidden"}} id={i}>
                                        <a href="#ChatPage" onClick={this.handleCurrentMatch} id={i}>
                                            <img style={imgStyle} src={photo} alt="MatchPhoto" id={i}/>
                                            <i style={{display:"inline"}} id={i}>
                                                <h3 id={i}>{item.name}</h3>
                                                {user+item.msg} 
                                            </i>
                                        </a>
                                    </li>);
                        })}
                    </ul>
                </nav>
            <div id="currentConversation" style={{marginLeft:"300px"}}>
            </div>            
            </div>
            <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                style={{display:"none"}} id="mySidebar">
                <a href="#About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact
                    us</a>
                <a href="#Destinations" onClick="firstpage_close()"
                    className="firstpage-bar-item firstpage-button">Destinations</a>
                <a href="#logout" className="firstpage-bar-item firstpage-button" onClick={this.handleLogOut} >Logout</a>
            </nav>

            <footer className="firstpage-center firstpage-light-grey firstpage-card" style={{paddingBottom:"1%",paddingTop:"10%"}} >
                <a href="#profile" id="ToTheTopButton" className="firstpage-button firstpage-light-grey" style={{marginLeft:"200px"}}><i
                        className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                <div className="firstpage-xlarge firstpage-section" style={{marginLeft:"200px"}}>
                    <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                </div>
                <div style={{marginLeft:"200px"}}>
                <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                        className="firstpage-hover-text-green" >FCT</a></p>
                        </div>
            </footer>
        </div>
        
        )

    }

}

export default ChatPage;
import React,{Component} from 'react';
import { Map, GoogleApiWrapper, Polyline , Marker,InfoWindow } from 'google-maps-react';
import { NewCourse } from './NewCourse.js';
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';
//import { withGoogleMap, GoogleMap, DirectionsRenderer } from "react-google-maps";
import $ from 'jquery';
import avatar from '../../images/avatar.png';

const mapStyles = {
    position:'fixed',
    width: '80%',
    height: '55%',
    marginLeft: '9%',
    marginTop: '1%',
    paddingRight:'-80%'    
  };

class CreateRoute extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          submit: false,
          photo:avatar,
          showingInfoWindow: false,
          activeMarker: {},
          load:false,
          initialCenter: [{
            title: "You are here",
              name: "Current Position",
              position: { 
                lat:38.741881, 
                lng: -9.212413
              }
          }],
          markers: [],
          selectedPlace: {},
          distance:0,
          existsMarkers: false
        }
        this.handleLogOut=this.handleLogOut.bind(this);
        this.showPosition = this.showPosition.bind(this);   
        this.addMarker = this.addMarker.bind(this);
        this.resetMarkers = this.resetMarkers.bind(this);
        this.showSubmit = this.showSubmit.bind(this);
    }
    
    //pos current
    async componentWillMount() {
      if (navigator.geolocation) {
        await navigator.geolocation.watchPosition(this.showPosition);
      }

    }
        
    showPosition(position) {
      //alert("aqui");
      const lat = position.coords.latitude;
      const lng = position.coords.longitude;
      this.setState({
          initialCenter:[
            {
              title: "You are here",
              name: "Current Position",
              position: { lat, lng }
            }
          ]
      });
      this.setState({load:true});
    }

    //VER O QUE METER DEPOIS
    addMarker(t, map, coord) {
      if(this.state.markers.length<=10){
        const { latLng } = coord;
        const lat = latLng.lat();
        const lng = latLng.lng();
        this.setState(previousState => {
          return {
            markers: [
              ...previousState.markers,
              {
                position: { lat, lng }
              }
            ]
          };
        });

      }else{
        alert("You need to be premium in order to choose more then 10 markers!");
      }
    }

    resetMarkers(){
      this.setState({markers:[]});
    }

    onMarkerClick = (props, marker, e) =>
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
    });

    distFrom() {
      const length = this.state.markers.length-1;
      const lat1= Math.abs(this.state.markers[0].position.lat);
      const lat2 = Math.abs(this.state.markers[length].position.lat);
      const lng1 = Math.abs(this.state.markers[0].position.lng);
      const lng2 = Math.abs(this.state.markers[length].position.lng);
      const earthRadius = 6371000; //meters
      //const dLat = (Math.PI/180)*(lat2 - lat1);
      //const dLng = Math.toRadians(lng2 - lng1);
      const dLat = Math.abs(lat2-lat1);
      const dLng = Math.abs(lng2-lng1);
      const a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                 Math.cos(lat1) * Math.cos(lat2) *
                 Math.sin(dLng/2) * Math.sin(dLng/2);
      const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      const dist = (earthRadius * c);
      this.setState({distance:dist});
      alert(dist);
  }
 
    showSubmit(){
      if(this.state.markers.length>1){
        this.setState({submit:true});
        this.distFrom();       
        document.querySelector("#newCourse").style.display = 'block';
      }else{
        alert("You need to choose at least two markers!");
      }
  }
  
    handleLogOut(e){
        e.preventDefault();
        $.ajax({
            type: "DELETE",
            url: "https://tour-tuga.appspot.com/logout",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            data:JSON.stringify({
                tokenID:sessionStorage.getItem('tokenID'),
                email:sessionStorage.getItem('email')}
            ),
            success: ()=> {
                    var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                    alert("See you later " + fullName)
                    sessionStorage.clear();
                    window.location.hash = "/";
            },
            error: function (data) {
                alert("Error: " + data.status);
                if(data.responseJSON.Error === 'InvalidToken'){
                  sessionStorage.clear();
                         window.location.hash = '/';                    

              }
            },
        })
    }

    componentDidMount(){
      var tokenID = sessionStorage.getItem("tokenID");
      var email = sessionStorage.getItem("email");
          $.ajax({
              type: "PUT",
              url: "https://tour-tuga.appspot.com/getuserinfo",
              contentType: "application/json; charset=utf-8",
              crossDomain: true,
              dataType: "json",
              data : JSON.stringify({tokenID:tokenID, email:email}),
              success: (data)=>{
                  if(data.error)
                      alert("No user is logged!");
                  else{
                      var decodedString = data.photo;
                      if(!decodedString.startsWith("data:image/jpeg;base64,")){
                          decodedString = "data:image/jpeg;base64,"+decodedString;
                      }
                      this.setState({
                      photo:decodedString
                      })   
                  }
              },
              error:(data)=> {
                  alert("Error: " + data.status);
                  if(data.responseJSON.Error === 'InvalidToken'){
                      sessionStorage.clear();
                             window.location.hash = '/';                    

                  }
              },
          })
      }

  

    render(){
      //const accessory = this.state.existsMarkers;
      const submit = this.state.submit;

        return(
            <div className="CreateRoutes firstpage-light-grey">
                <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About"  className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <div className="dropdown">
                            <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                            </button>
                            <div className="dropdown-content" id="myDropdown">
                                <a href="#Profile">Profile</a>
                                <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>

            <div id="newCourse" className="modal">
            {submit &&
              <NewCourse distance = {this.state.distance}/>
            }
            </div>
            
              <div style={{marginBottom:"31%"}}> 

                <div style={{marginLeft:"9%", display:"inline-flex" , marginTop:"1.5%"}}>
                    <Button variant="success" size="lg" onClick={this.showSubmit} block style={{width:"35%"}}>
                      Accept selected waypoints
                    </Button>
                      <h5 style={{textAlign:"center", marginTop:"2%", width:"600px"}}> 
                        Right-click new waypoints (max 10) directly on the map! 
                      </h5>
                    <Button variant="danger" size="lg" onClick={this.resetMarkers} block style={{width:"35%"}}>
                      Clear selected waypoints
                    </Button>
                </div>
                    <Map                                                
                        onRightclick={this.addMarker}
                        google={this.props.google}
                        zoom={14}
                        style={mapStyles}
                        center={{lat:this.state.initialCenter[0].position.lat,
                                 lng:this.state.initialCenter[0].position.lng}}
                            >
                         {this.state.markers.map((marker, index) => (
                          <Marker draggable={true}
                            onClick={this.onMarkerClick}
                            title={index+1}
                            name={index+1}
                            key={index}
                            position={marker.position}
                          />
                        ))}
                        
                        <InfoWindow
                          marker={this.state.activeMarker}
                          visible={this.state.showingInfoWindow}>
                            <div>
                              <h1>{this.state.selectedPlace.name}</h1>
                            </div>
                        </InfoWindow>
                    </Map>
                </div>
                            

                <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                            style={{display:"none"}} id="mySidebar">
                            <a href="#about" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                            <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                            <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact us</a>
                            <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                            <a href="#Register" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Sign up</a>
                            <a href="#logout" className="firstpage-bar-item firstpage-button" onClick={this.handleLogOut}>Logout</a>
                        </nav>

                        <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%"}}>
                            <a href="#about" id="ToTheTopButton" className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>
            </div>

        )


    }



}

//export default CreateRoute;
export default GoogleApiWrapper({
    apiKey: 'AIzaSyDl5GCkOo277llEO4cAeZBwvUzEweiQtvg'
  })(CreateRoute);

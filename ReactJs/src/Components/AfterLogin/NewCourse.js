import React,{Component} from 'react';
import $ from 'jquery';

export class NewCourse extends Component{
    constructor(props){
        super(props);
        this.state ={
            title:'',
            description:'',
            difficulty:'',
            distance:this.props.distance,
            topic:{},
            markers:[],
            img:'',
            email:'',
            estimatedTime:0,
            newMessage:false
        }
        this.hideWindow = this.hideWindow.bind(this);
        this.handleCourseSubmit = this.handleCourseSubmit.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    hideWindow(){
        document.querySelector("#newCourse").style.display = 'none';
    }

    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
    }

    handleCourseSubmit(e){
        const tokenID = sessionStorage.getItem('tokenID');
        e.preventDefault();   
        $.ajax({
            type: "POST",
            url: "https://tour-tuga.appspot.com/createroute",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            dataType: "json",
            data : JSON.stringify({
                titulo:this.state.title,
                descricao:this.state.description,
                dificuldade:this.state.difficulty,
                distancia:this.state.distance,
                topicos:this.state.topic,
                markers:this.state.markers,
                imagens:this.state.img,
                email:this.state.email,
                time:this.state.estimatedTime,
                tokenID:tokenID,
            }),
            success: (data)=>{
                //sucesso mandar para outra pagina
                alert("Route created!");
                if(data.newMessage)
                    this.setState({newMessage:true});
            },
            error:(data)=> {
                alert("Error: " + data.status);
                if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                           window.location.hash = '/';                    
                }
            },
        })
    }
render(){
    return(    
        <form name="loginRequest" id="LGcontainer" className="modal-content animate" onSubmit = {this.handleLoginSubmit}>
            <div className="imgcontainer">
                <span onClick={this.hideWindow} className="close"
                title="Close Modal">&times;</span>
            </div>
            <div className="container" id="LGsection">
                <section className="loginform cf">
                    <legend style={{textAlign:"center"}}>New Route</legend>
                    <label htmlFor="title">Title</label> 
                    <input type="text" name="title" placeholder="Best Route Ever" onChange ={this.handleTextChange} required/> 
                    <label htmlFor="description">Description</label>
                    <input type="text" name="description" placeholder="Google Maps Who?" onChange = {this.handleTextChange} required/>
                    <label style={{paddingRight:"2%"}} htmlFor="difficulty">Route difficulty</label>
                    <select id="difficulty" name="difficulty" placeholder="1" onChange = {this.handleTextChange} required>
                        <option value="1"> 1 (Easiest) </option>
                        <option value="2"> 2 </option>
                        <option value="3"> 3 </option>
                        <option value="4"> 4 </option>
                        <option value="5"> 5 </option>
                        <option value="6"> 6 </option>
                        <option value="7"> 7 </option>
                        <option value="8"> 8 </option>
                        <option value="9"> 9 </option>
                        <option value="10"> 10 (Hardest) </option>
                    </select><br/>                    
                    <label htmlFor="distance">Distance (informative and locked)</label>
                    <input type="text" name="distance" placeholder="DISTANCE!!!" onChange = {this.handleTextChange} required/>
                    <label style={{paddingRight:"2%"}} htmlFor="topic">Type of tourism</label>
                    <ul float="center" >
                        <label className="FTcheck" style={{width:"10%", marginLeft:"7%"}}>
                            <input onChange={this.handleTextChange} id="1" type="checkbox" 
                            value="Adventure "/>
                            <span class="checkmark"></span>
                        Adventure</label>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"20%"}}>
                            <input onChange={this.handleTextChange} id="2" type="checkbox" 
                            value=" Agritourism "/>
                            <span class="checkmark"></span>
                        Agritourism</label><br/>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"7%"}}>
                            <input onChange={this.handleTextChange} id="3" type="checkbox" 
                            value=" Art tourism "/>
                            <span class="checkmark"></span>
                        Art tourism</label>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"18.8%"}}>
                            <input onChange={this.handleTextChange} id="4" type="checkbox" 
                            value=" Culinary tourism "/>
                            <span class="checkmark"></span>
                        Culinary tourism</label><br/>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"7%"}}>
                            <input onChange={this.handleTextChange} id="5" type="checkbox" 
                            value=" Cultural tourism "/>
                            <span class="checkmark"></span>
                        Cultural tourism</label>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"9.3%"}}>
                            <input onChange={this.handleTextChange} id="6" type="checkbox" 
                            value=" Eco/geo tourism "/>
                            <span class="checkmark"></span>
                        Eco/geo tourism</label><br/>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"7%"}}>
                            <input onChange={this.handleTextChange} id="7" type="checkbox" 
                            value=" Music tourism "/>
                            <span class="checkmark"></span>
                        Music tourism</label>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"13%"}}>
                            <input onChange={this.handleTextChange} id="8" type="checkbox" 
                            value=" Nautical tourism "/>
                            <span class="checkmark"></span>
                        Nautical tourism</label><br/>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"7%"}}>
                            <input onChange={this.handleTextChange} id="9" type="checkbox" 
                            value=" Religious tourism "/>
                            <span class="checkmark"></span>
                        Religious tourism</label>
                        <label className="FTcheck" style={{width:"10%", marginLeft:"6.3%"}}>
                            <input onChange={this.handleTextChange} id="10" type="checkbox" 
                            value=" Rural tourism"/>
                            <span class="checkmark"></span>
                        Rural tourism</label>
                    </ul>
                    <br/>                    
                    <label htmlFor="markers">Markers?</label>   
                    <br/>                 
                    <label htmlFor="estimatedTime">Estimated Time?</label>                
                    
                    <br/>
                    <button onClick ={this.handleCourseSubmit}>Create</button>
                    <button style={{float:"right", backgroundColor:"red"}} onClick ={this.handleCourseSubmit}>Reset</button>
                    <br/>
                </section>
            </div>
         </form>         
    );
    
}
    
    
}


export default NewCourse;
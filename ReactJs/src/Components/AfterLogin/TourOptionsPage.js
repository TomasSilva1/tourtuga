import React,{Component} from 'react';
import '../AfterLogin/css/profilepage.css';
import $ from 'jquery';
import avatar from '../../images/avatar.png';

var checked=0;
//PEDIR PARA RECEBER OS ESCOLHIDOS, METER EM TABLE
class TourOptionsPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            options:[],
            photo:avatar,
            newMessage:false
        }
        this.chooseOptions = this.chooseOptions.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.handleFavTypesSubmit = this.handleFavTypesSubmit.bind(this);
    }

    componentDidMount(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({tokenID:tokenID, email:email}),
                success: (data)=>{
                    var photo = data.photo;
                    if(!data.photo){
                        photo=avatar;
                    }
                    if(data.newMessage)
                        this.setState({newMessage:data.newMessage});
                    if(data.favTourism != null){
                        this.setState({
                            options:data.favTourism,photo:photo
                        })
                        for(var i=1;i<=this.state.options.length;i++){
                            var value = document.getElementById(i).getAttribute('value');
                            if(this.state.options.indexOf(value) > -1){
                                document.getElementById(i).checked = true;
                                checked++;
                            }
                        }
                    }
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                               window.location.hash = '/';                    

                    }
                },
            })
        }


    chooseOptions(e){
        const target = e.target;
        const text = target.value;
        if (checked < 3 || !target.checked) {
            if (target.checked) {
                this.state.options.push(text);
                checked++;
            } else {
                this.state.options.pop(text);
                checked--;
            }
        } else{
            target.checked = false;
            this.state.options.pop(text);
            alert("All three have been choosen");
         }
    }

    handleFavTypesSubmit(e){
        e.preventDefault();
        if(checked < 3){
            alert("You need to choose "+3-checked+" more");
        }else{
            var tokenID = sessionStorage.getItem('tokenID');
            var email = sessionStorage.getItem('email');
                $.ajax({
                    type: "PUT",
                    url: "https://tour-tuga.appspot.com/selectFavTypes",
                    contentType: "application/json; charset=utf-8",
                    crossDomain: true,
                    dataType: "json",
                    data : JSON.stringify({
                        tokenID:tokenID, email:email,favoriteTypes:this.state.options
                    }),
                    success: (data)=>{
                        if(data.newMessage)
                            this.setState({newMessage:data.newMessage});
                        alert("User updated favTypes");
                        window.location.hash = '/Profile';
                    },
                    error:(data)=> {
                        alert("Error: " + data.status);
                        if(data.responseJSON.Error === 'InvalidToken'){
                            sessionStorage.clear();
                                   window.location.hash = '/';                    

                        }
                    },
                })
        }
    }

    handleLogOut(e){
        e.preventDefault();
        $.ajax({
            type: "DELETE",
            url: "https://tour-tuga.appspot.com/logout",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            data:JSON.stringify({
                tokenID:sessionStorage.getItem('tokenID'),
                email:sessionStorage.getItem('email')}
            ),
            success: ()=> {
                    var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                    alert("See you later " + fullName)
                    sessionStorage.clear();
                    window.location.hash = "/";
            },
            error: function (data) {
                alert("Error: " + data.status);
                if(data.responseJSON.Error === 'InvalidToken'){
                    sessionStorage.clear();
                           window.location.hash = '/';                    

                }
            },
        })
    }

    
    render(){
        const newMessage=this.state.newMessage;
        return(
                <div className="TourOptionsPage">
                    <div className="firstpage-top">
                    <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About"  className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <div className="dropdown">
                            <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                            </button>
                            <div className="dropdown-content" id="myDropdown">
                                <a href="#Profile">Profile</a>
                                <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                            </div>
                        </div>
                        {!newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                        </div>
                        }
                        {newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                        </div>
                        }
                    </div>
                </div>
                    </div>
                    <div id="tourOptions" className="firstpage-light-grey" style={{paddingTop:"4%"}}>
                        <form name="tourRequest">
                            <div className="container">
                                <section className="tourOptions cf">
                                    <h1>Choose your first, second and third favourite types of tourism:</h1>
                                    <hr/>
                                    <ul >
                                        <label className="FTcheck">
                                            <input onChange={this.chooseOptions} id="1" type="checkbox" 
                                            value="Adventure "/>
                                            <span class="checkmark"></span>
                                        Adventure</label>
                                        <label className="FTcheck" style={{left:"10%"}}>
                                            <input onChange={this.chooseOptions} id="2" type="checkbox" 
                                            value=" Agritourism "/>
                                            <span class="checkmark"></span>
                                        Agritourism</label>
                                        <label className="FTcheck" style={{left:"20%"}}>
                                            <input onChange={this.chooseOptions} id="3" type="checkbox" 
                                            value=" Art tourism "/>
                                            <span class="checkmark"></span>
                                        Art tourism</label>
                                        <label className="FTcheck" style={{left:"30%"}}>
                                            <input onChange={this.chooseOptions} id="4" type="checkbox" 
                                            value=" Culinary tourism "/>
                                            <span class="checkmark"></span>
                                        Culinary tourism</label>
                                        <br/>
                                        <label className="FTcheck">
                                            <input onChange={this.chooseOptions} id="5" type="checkbox" 
                                            value=" Cultural tourism "/>
                                            <span class="checkmark"></span>
                                        Cultural tourism</label>
                                        <label className="FTcheck" style={{left:"5.21%"}}>
                                            <input onChange={this.chooseOptions} id="6" type="checkbox" 
                                            value=" Eco/geo tourism "/>
                                            <span class="checkmark"></span>
                                        Eco/geo tourism</label>
                                        <label className="FTcheck" style={{left:"10.9%"}}>
                                            <input onChange={this.chooseOptions} id="7" type="checkbox" 
                                            value=" Music tourism "/>
                                            <span class="checkmark"></span>
                                        Music tourism</label>
                                        <label className="FTcheck" style={{left:"18.25%"}}>
                                            <input onChange={this.chooseOptions} id="8" type="checkbox" 
                                            value=" Nautical tourism "/>
                                            <span class="checkmark"></span>
                                        Nautical tourism</label>
                                        <br/>
                                        <label className="FTcheck">
                                            <input onChange={this.chooseOptions} id="9" type="checkbox" 
                                            value=" Religious tourism "/>
                                            <span class="checkmark"></span>
                                        Religious tourism</label>
                                        <label className="FTcheck" style={{left:"3.85%"}}>
                                            <input onChange={this.chooseOptions} id="10" type="checkbox" 
                                            value=" Rural tourism"/>
                                            <span class="checkmark"></span>
                                        Rural tourism</label>
                                    </ul>
                                    <br/>
                                    <button type="submit" onClick={this.handleFavTypesSubmit}>Submit Options</button>
                                    <a href="#Profile" style={{paddingLeft: "10px"}}>Go to your Profile!</a>
                                </section>
                            </div>
                        </form>
                    </div>


                    <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                        style={{display:"none"}} id="mySidebar">
                        <a href="#About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact
                            us</a>
                        <a href="#Destinations" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Destinations</a>
                        <a href="#logout" className="firstpage-bar-item firstpage-button" onClick="">Logout</a>
                    </nav>

                    <footer className="firstpage-center firstpage-light-grey" style={{paddingBottom:"1%",paddingTop:"2%"}}>
                            <a href="#about" id="ToTheTopButton"  className="firstpage-button firstpage-light-grey"><i
                                    className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                            <div className="firstpage-xlarge firstpage-section">
                                <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                                <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                            </div>
                            <div >
                            <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                                    className="firstpage-hover-text-green" >FCT</a></p>
                                    </div>
                        </footer>

                 </div>



        )

    }


}

export default TourOptionsPage;
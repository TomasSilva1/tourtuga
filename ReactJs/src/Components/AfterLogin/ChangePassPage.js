import React,{Component} from 'react';
import '../AfterLogin/css/profilepage.css';
import './css/style.default.css';
import avatar from '../../images/avatar.png';
import $ from 'jquery';

//FAZER COMPONENTS PARA CHANGE PASSWORD,ETC, ALINHAR OS ICONS
//FAZER UM AVISO PARA O CC, NAO E OBRIGATORIO MAS PARA USAR SOS E TINDER TEM QUE METER
class ChangePassPage extends Component{
    constructor(props){
        super(props);
        this.state ={
            firstname:'',
            lastname:'',
            email:'',
            photo: avatar,
            oldPassword:'',
            password:'',
            newPassword:'',
            confirmation:'',
            newMessage:false
        }
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleUserUpdate = this.handleUserUpdate.bind(this);
        this.handleDeleteAccount = this.handleDeleteAccount.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
    }

    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
        
    }

    componentDidMount(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({tokenID:tokenID, email:email}),
                success: (data)=>{
                    var photo = data.photo;
                    if(!data.photo){
                        photo=avatar;
                    }
                    this.setState({
                    firstname :data.firstname,
                    lastname :data.lastname,
                    email:data.email,
                    oldPassword:data.password,
                    photo:photo,
                    newMessage:data.newMessage
                    });
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                               window.location.hash = '/';                    

                    }
                },
            })
    }

    handleUserUpdate(e){
        e.preventDefault();
        var crypto = require('crypto');
        var password = crypto.createHash('sha512').update(this.state.password).digest('hex');
        var tokenID = sessionStorage.getItem('tokenID');
        if(this.state.oldPassword === password && this.state.newPassword === this.state.confirmation){
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/updateUser",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    password:this.state.newPassword,
                    email:this.state.email,
                    tokenID:tokenID
                }),
                success: (data)=>{
                    if(data.newMessage)
                        this.setState({newMessage:data.newMessage});
                    alert("Password changed successfully");
                    window.location.hash = '/Profile';
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    
                    }
                },
            })
        }else if(this.state.newPassword !== this.state.confirmation){
            alert("New Password and Confirmation are different");
        }else if(this.state.oldPassword !== password){
            alert("The password is incorrect");
        }
    }

        handleDeleteAccount(e){
            var choice = window.confirm("If you are sure you want to delete your account press OK!");
            if(choice===true){
                e.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "https://tour-tuga.appspot.com/deleteUser",
                    contentType: "application/json; charset=utf-8",
                    crossDomain: true,
                    data:JSON.stringify({
                        tokenID:sessionStorage.getItem('tokenID'),
                        email:sessionStorage.getItem('email')
                    }),
                    success: ()=>{
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("Hope to see you in the future " + fullName);
                        sessionStorage.clear();
                        window.location.hash = "/";
                    },
                    error:(data)=>{
                        alert("Error: "+data.status);
                        if(data.responseJSON.Error === 'InvalidToken'){
                            sessionStorage.clear();
                                   window.location.hash = '/';                    

                        }
                    }
                })
            }
        }

        handleLogOut(e){
            e.preventDefault();
            $.ajax({
                type: "DELETE",
                url: "https://tour-tuga.appspot.com/logout",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                data:JSON.stringify({
                    tokenID:sessionStorage.getItem('tokenID'),
                    email:sessionStorage.getItem('email')}
                ),
                success: ()=> {
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("See you later " + fullName);
                        sessionStorage.clear();
                        window.location.hash = "/";
                },
                error: function (data) {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                               window.location.hash = '/';                    
                    }
                },
            })
        }

    render(){
        const newMessage=this.state.newMessage;
        return(
        <div className="ChangePassPage">
            <div className="firstpage-top ">
            <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About"  className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <div className="dropdown">
                            <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                            </button>
                            <div className="dropdown-content" id="myDropdown">
                                <a href="#Profile">Profile</a>
                                <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                            </div>
                        </div>
                        {!newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                        </div>
                        }
                        {newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                        </div>
                        }
                    </div>
                </div>
            </div>
            
            <div className="firstpage-light-grey" id="profile" style={{paddingBottom:"20px"}}>
            <nav className="side-navbar firstpage-light-grey" id="PPsideBar" style={{marginLeft:"9%"}}>
                    <div className="sidebar-header d-flex align-items-center">
                        <div>
                        <img src={this.state.photo} alt="profileAvatar" id="PSPprofileAvatar" className="img-fluid rounded-circle"/>
                        </div>
                    </div>
                    <div className="title">
                            <h1 className="h4" >{this.state.firstname} {this.state.lastname}</h1>
                        </div>
                    <ul className="list-unstyled" id="nav" >
                        <li><a href="#Profile"> <i className="	fa fa-address-card-o"></i>Update Profile</a></li>
                        <li><a href="#ChangePass"> <i className="fa fa-lock"></i>Change Password</a></li>
                        <li><a href="#FavTourism"> 
                        <i className="fa fa-map-marker"></i>Change Fav-Tourism</a></li>
                        <li><a href="#ChatPage"> <i className="fa fa-envelope-o" style={{marginLeft:"1px"}}></i>Matches</a></li>
                        <li onClick={this.handleDeleteAccount}><a href="#Profile"> <i className="fa fa-close" style={{marginLeft:"1px"}}></i>Delete Account</a></li>
                    </ul>
                </nav>
                <div className="firstpage-light-grey " id="PPstatus">
                    <div className="imgcontainer firstpage-light-grey">
                        <input name="photo" type="file-input" id="file1" accept="image/*" capture style={{display:"none"}}/>
                        <div className="image-upload">
                            <label htmlFor="file-input">
                                <div className="imageContainer" id="PPimgDiv">
                                    <img src={this.state.photo} alt="ProfileIcon" id="PPprofileAvatar" className="image"/>
                                    <div className="overlay">
                                    <i id="uploadText">Upload Photo</i>
                                     </div>
                                </div>
                            </label>
                            <input name ="photo" id="file-input" type="file" accept="image/*" onChange={this.handleImageChange} />
                        </div>
                    </div>
                    <h2 style={{textAlign: "center", marginBottom:"-2%"}}>Change Password</h2>
                    <br/><br/><br/>
                    <div className="row">
                        <label id="CPLCuurent">Current Password</label>
                        <input id="CPCurrent" name="password" type="password" placeholdes="currentPassword" onChange={this.handleTextChange} value={this.state.password}>
                        </input>
                        <br/>
                        <label id="CPLNew">New Password</label>
                        <input id="CPNew" name="newPassword" type="password" placeholdes="NewPassword" onChange={this.handleTextChange} value={this.state.newPassword}>
                        </input>
                        <br/>
                        <label id="CPLConfirmation">Confirm New Password</label>
                        <input id="CPConfirmation" name="confirmation" type="password" placeholdes="Confirm" onChange={this.handleTextChange} value={this.state.confirmation}>
                        </input>
                        <button id="PPbutton"onClick={this.handleUserUpdate} type="submit">Update</button>
                    </div>
                </div>
            </div>
            
            
            <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                style={{display:"none"}} id="mySidebar">
                <a href="#About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact
                    us</a>
                <a href="#Destinations" onClick="firstpage_close()"
                    className="firstpage-bar-item firstpage-button">Destinations</a>
                <a href="#logout" className="firstpage-bar-item firstpage-button" onClick={this.handleLogOut} >Logout</a>
            </nav>

            <footer className="firstpage-center firstpage-light-grey firstpage-card"style={{paddingBottom:"1%"}} >
                <a href="#profile" id="ToTheTopButton" className="firstpage-button firstpage-light-grey" style={{marginLeft:"200px"}}><i
                        className="fa fa-arrow-up firstpage-margin-right"></i>To the top</a>
                <div className="firstpage-xlarge firstpage-section" style={{marginLeft:"200px"}}>
                    <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                </div>
                <div style={{marginLeft:"200px"}}>
                <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                        className="firstpage-hover-text-green" >FCT</a></p>
                        </div>
            </footer>
        </div>


        )

    }

}

export default ChangePassPage;
import React, { Component } from 'react';
import ReactFlagsSelect from 'react-flags-select';
import '../AfterLogin/css/profilepage.css';
import avatar from '../../images/avatar.png';
import $ from 'jquery';

class ProfilePage extends Component{
    constructor(props){
        super(props);
        this.state ={
            firstname:'',
            lastname:'',
            email:'',
            age:0,
            telemovel:'',
            photo: avatar,
            country:'',
            password:'',
            privado:'',
            newMessage:false
        }
        this.handleImageChange = this.handleImageChange.bind(this);
        this.otherChoice = this.otherChoice.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleUserUpdate = this.handleUserUpdate.bind(this);
        this.handleDeleteAccount = this.handleDeleteAccount.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.toogleDropdown = this.toogleDropdown.bind(this);
        this.removeShow = this.removeShow.bind(this);
    }


    handleImageChange(e) {
        var postData = e.target.files[0];
        var bucket = "tour-tuga.appspot.com";
        var filename = postData.name;
        if (bucket == null || bucket == "" || filename == null || filename == "") {
            alert("Both Bucket and FileName are required");
            return false;
        } else {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", postData.type+";charset=UTF-8");
            fetch("/gcs/" + bucket + "/" + filename,{
                method : "POST",
                headers: myHeaders,
                body: postData
            }).then(response => {
                console.log(response);
                this.setState({photo:response.url});
            })
        }
    }
/*
    handleImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            localStorage.setItem('photo',reader.result);    
            this.setState({
            photo:reader.result
            });
        }
      }
*/
    otherChoice(privado){
        if(privado ==="privado"){
            document.getElementById("op1").innerText ="Privado";
            document.getElementById("op1").value = "private";
            document.getElementById("op2").innerText="Publico";
            document.getElementById("op2").value = "public";

        }else{
            document.getElementById("op2").innerText ="Privado";
            document.getElementById("op2").value = "private";
            document.getElementById("op1").innerText="Publico";
            document.getElementById("op1").value = "public";
        }
    }

    
    handleTextChange(e){
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({...this.state, [name]: value});
        
    }

    componentDidMount(){
        var tokenID = sessionStorage.getItem("tokenID");
        var email = sessionStorage.getItem("email");
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/getuserinfo",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({tokenID:tokenID, email:email}),
                success: (data)=>{
                    var photo = data.photo;
                    if(!data.photo){
                        photo=avatar;
                    }else if(data.photo !== sessionStorage.getItem('photo')){
                        localStorage.setItem('photo',data.photo);
                    }
                    this.setState({
                        firstname :data.firstname,
                        lastname :data.lastname,
                        email : data.email,
                        age:data.age,
                        telemovel :data.telemovel,
                        country: data.country,
                        privado: data.privado,
                        photo:photo,
                        newMessage:data.newMessage
                    })
                    this.otherChoice(data.privado);
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    console.log(data);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    

                    }
                },
            })
        }

    handleUserUpdate(e){
        e.preventDefault();
        var tokenID = sessionStorage.getItem('tokenID');
        var op = $('#privado option:selected').val();
            $.ajax({
                type: "PUT",
                url: "https://tour-tuga.appspot.com/updateUser",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                dataType: "json",
                data : JSON.stringify({
                    firstname:this.state.firstname, lastname:this.state.lastname,
                    email:this.state.email, photo:this.state.photo , privado:op,
                    age:this.state.age,telemovel:this.state.telemovel, 
                    country:this.state.country, tokenID:tokenID
                }),
                success: (data)=>{
                    if(data.newMessage)
                        this.setState({newMessage:data.newMessage});
                    alert("User updated");
                    window.location.hash = '/Profile';
                },
                error:(data)=> {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    

                    }
                },
            })
        }

        handleDeleteAccount(e){
            var choice = window.confirm("If you are sure you want to delete your account press OK!");
            if(choice===true){
            e.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "https://tour-tuga.appspot.com/deleteUser",
                    contentType: "application/json; charset=utf-8",
                    crossDomain: true,
                    data:JSON.stringify({
                        tokenID:sessionStorage.getItem('tokenID'),
                        email:sessionStorage.getItem('email')
                    }),
                    success: ()=>{
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("Hope to see you again " + fullName);
                        sessionStorage.clear();
                        window.location.hash = "/";
                    },
                    error:(data)=> {
                        alert("Error: " + data.status);
                        if(data.responseJSON.Error === 'InvalidToken'){
                            sessionStorage.clear();
                            window.location.hash = '/';                    
                        }
                    },
                })
            }
        }

        handleLogOut(e){
            e.preventDefault();
            $.ajax({
                type: "DELETE",
                url: "https://tour-tuga.appspot.com/logout",
                contentType: "application/json; charset=utf-8",
                crossDomain: true,
                data:JSON.stringify({
                    tokenID:sessionStorage.getItem('tokenID'),
                    email:sessionStorage.getItem('email')}
                ),
                success: ()=> {
                        var fullName = sessionStorage.getItem("firstname") + " " + sessionStorage.getItem("lastname");
                        alert("See you later " + fullName);
                        sessionStorage.clear();
                        window.location.hash = "/";
                },
                error: function (data) {
                    alert("Error: " + data.status);
                    if(data.responseJSON.Error === 'InvalidToken'){
                        sessionStorage.clear();
                        window.location.hash = '/';                    

                    }
                },
            })
        }

        toogleDropdown() {
            document.getElementById("myDropdown").classList.toggle("show");
        }
        removeShow(e) {
            if (!e.target.matches('.dropbtn')) {
                var myDropdown = document.getElementById("myDropdown");
                if (myDropdown.classList.contains('show')) {
                    myDropdown.classList.remove('show');
                }
            }
        }
       

    render(){
        const newMessage = this.state.newMessage;
        return(  
        <div className="ProfilePage">
            <div className="firstpage-top ">
                <div className="firstpage-white firstpage-card" id="myNavbar">
                    <a id="logoPart" href="/" className="firstpage-bar-item firstpage-button" style={{marginLeft:"9%"}}>
                    </a>
                    <div className="firstpage-right firstpage-hide-small" style={{marginRight:"9%"}}>
                        <a href="#About"  className="firstpage-bar-item firstpage-button">About</a>
                        <a href="#faq"  className="firstpage-bar-item firstpage-button">FAQ</a>
                        <a href="#ContactUs"  className="firstpage-bar-item firstpage-button">Contact us</a>
                        <a href="#Destinations" className="firstpage-bar-item firstpage-button ">Destinations</a>
                        <div className="dropdown">
                            <button id="PPDropdown" href="#Profile" className="firstpage-bar-item firstpage-button dropbtn" 
                                onClick={this.toogleDropdown} style={{backgroundImage:"url("+this.state.photo+")"}}>
                            </button>
                            <div className="dropdown-content" id="myDropdown">
                                <a href="#Profile">Profile</a>
                                <a href="#logout" onClick={this.handleLogOut}>Logout</a>
                            </div>
                        </div>
                        {!newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications</i></a>
                        </div>
                        }
                        {newMessage &&
                        <div className="firstpage-right">
                            <a href="#Profile" style={{marginTop:"50%"}}><i className="material-icons">notifications_active</i></a>
                        </div>
                        }
                    </div>
                </div>
            </div>
            
            <div className="firstpage-light-grey" id="profile" style={{paddingBottom:"20px"}}>
            <nav className="side-navbar firstpage-light-grey" id="PPsideBar" style={{marginLeft:"9%"}}>
                    <div className="sidebar-header d-flex align-items-center">
                        <div>
                        <img src={this.state.photo} alt="profileAvatar" id="PSPprofileAvatar" className="img-fluid rounded-circle"/>
                        </div>
                    </div>
                    <div className="title">
                            <h1 className="h4" >{this.state.firstname} {this.state.lastname}</h1>
                        </div>
                    <ul className="list-unstyled" id="nav" >
                        <li><a href="#Profile"> <i className="	fa fa-address-card-o"></i>Update Profile</a></li>
                        <li><a href="#ChangePass"> <i className="fa fa-lock"></i>Change Password</a></li>
                        <li><a href="#FavTourism"> 
                        <i className="fa fa-map-marker"></i>Change Fav-Tourism</a></li>
                        <li><a href="#ChatPage"> <i className="fa fa-envelope-o" style={{marginLeft:"1px"}}></i>Matches</a></li>
                        <li onClick={this.handleDeleteAccount}><a href="#Profile"> <i className="fa fa-close" style={{marginLeft:"1px"}}></i>Delete Account</a></li>
                    </ul>
                </nav>
                <div className="firstpage-light-grey " id="PPstatus">
                    <div className="imgcontainer firstpage-light-grey">
                        <input name="photo" type="file-input" id="file1" accept="image/*" capture style={{display:"none"}}/>
                        <div className="image-upload">
                            <label htmlFor="file-input">
                                <div className="imageContainer" id="PPimgDiv">
                                    <img src={this.state.photo} alt="ProfileIcon" id="PPprofileAvatar" className="image"/>
                                    <div className="overlay">
                                    <i id="uploadText">Upload Photo</i>
                                     </div>
                                </div>
                            </label>
                            <input name ="photo" id="file-input" type="file" accept="image/*" onChange={this.handleImageChange} />
                        </div>
                    </div>
                    <h2 style={{textAlign: "center", marginBottom:"-2%"}}>Account Information</h2>
                    <div className="row">
                        <section className="update_user_infoform cf firstpage-light-grey">
                            <p><label htmlFor="firstname">First Name</label>
                                <input name="firstname" id="firstname" className="firstpage-input-name firstpage-border"
                                    type="text" onChange ={this.handleTextChange} value={this.state.firstname}/>
                                <label htmlFor="lastname">Last Name</label>
                                <input name="lastname" id="lastname" className="firstpage-input-email firstpage-border"
                                    type="text" onChange ={this.handleTextChange} value={this.state.lastname}/>
                            </p>
                            <p><label htmlFor="email">Email</label>
                                <input name="email" id="email" className="firstpage-input-name firstpage-border"
                                type="email" onChange ={this.handleTextChange} value={this.state.email}/>
                                
                            </p>
                            <p><label htmlFor="mobilephone">MobilePhone</label>
                                <input name="telemovel" id="telemovel" className="firstpage-input-email firstpage-border"
                                    type="tel" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" onChange ={this.handleTextChange}
                                    value = {this.state.telemovel}/>
                            </p>
                            <p>
                                
                                 <label htmlFor="age">Age</label>
                                 <br/>
                                <input name ="age" type="number" id="PPage" min="16" max="99" onChange={this.handleTextChange}
                                    value = {this.state.age} required ></input>

                                <br/>
                                <label htmlFor ="country">Country</label>    
                                <div className="menu-flags">
                                    <ReactFlagsSelect className="menu-flags" alignOptions="left" optionsSize={15} 
                                    selectedSize={25} searchable={true} searchPlaceholder="Search for a country"
                                    onSelect={this.handleSelectFlag} showSelectedLabel={false} defaultCountry="US" />
                                </div>
                                
                            </p>
                            <p>
                            <br/>
                                <label htmlFor="privado">Profile</label>
                                <br/>
                                    <select id="PPprofile" name="privado" onChange ={this.handleTextChange}>
                                        <option id="op1"></option>
                                        <option id="op2"></option>
                                    </select>
                            </p>
                                <button id="PPbutton"onClick={this.handleUserUpdate} type="submit">Update</button>
                            
                        </section>
                    </div>
                </div>
            </div>
            
            
            <nav className="firstpage-sidebar firstpage-bar-block firstpage-black firstpage-card firstpage-animate-left firstpage-hide-medium firstpage-hide-large"
                style={{display:"none"}} id="mySidebar">
                <a href="#About" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">About</a>
                <a href="#faq" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">FAQ</a>
                <a href="#ContactUs" onClick="firstpage_close()" className="firstpage-bar-item firstpage-button">Contact
                    us</a>
                <a href="#Destinations" onClick="firstpage_close()"
                    className="firstpage-bar-item firstpage-button">Destinations</a>
                <a href="#logout" className="firstpage-bar-item firstpage-button" onClick={this.handleLogOut} >Logout</a>
            </nav>

            <footer className="firstpage-center firstpage-light-grey firstpage-card"style={{paddingBottom:"1%"}} >
                <a href="#profile" id="ToTheTopButton" className="firstpage-button firstpage-light-grey" style={{marginLeft:"200px"}}><i
                        className="fa fa-arrow-up firstpage-margin-right "></i>To the top</a>
                <div className="firstpage-xlarge firstpage-section" style={{marginLeft:"200px"}}>
                    <a href="https://www.facebook.com/"><i className="fa fa-facebook-official firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-instagram firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-pinterest-p firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-twitter firstpage-hover-opacity"></i></a>
                    <a href="https://www.facebook.com/"><i className="fa fa-linkedin firstpage-hover-opacity"></i></a>
                </div>
                <div style={{marginLeft:"200px"}}>
                <p>Powered by <a href="https://www.fct.unl.pt/" title="FCT"  rel="noopener noreferrer" target="_blank"
                        className="firstpage-hover-text-green" >FCT</a></p>
                        </div>
            </footer>
        </div>


        )

    }

}

export default ProfilePage;
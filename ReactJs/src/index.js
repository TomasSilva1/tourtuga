import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import serviceWorker from './serviceWorker';
import firebase from 'firebase';

ReactDOM.render(<App />, document.getElementById('root'));

var firebaseConfig = {
    apiKey: "AIzaSyDl5GCkOo277llEO4cAeZBwvUzEweiQtvg",
    authDomain: "tour-tuga.firebaseapp.com",
    databaseURL: "https://tour-tuga.firebaseio.com/",
    projectId: "tour-tuga",
    storageBucket: "tour-tuga.appspot.com",
    messagingSenderId: "1021351712886",
    appId: "1:1021351712886:web:d532faa69e7d4af8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker();

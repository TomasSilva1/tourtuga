import React, {Component} from 'react';
import{HashRouter, Route, Switch} from 'react-router-dom';

import FirstPage from "./Components/BeforeLogin/FirstPage";
import RegisterPage from './Components/BeforeLogin/RegisterPage';
import ContactPage from './Components/BeforeLogin/ContactPage';
import AboutPage from './Components/BeforeLogin/AboutPage';
import ProfilePage from './Components/AfterLogin/ProfilePage';
import TourOptionsPage from './Components/AfterLogin/TourOptionsPage';
import DestinationsPage from './Components/BeforeLogin/DestinationsPage'; 
import CreateRoutes from './Components/AfterLogin/CreateRoutes';
import ChangePass from './Components/AfterLogin/ChangePassPage';
import ChatPage from './Components/AfterLogin/ChatPage';
import RecomDestinations from './Components/BeforeLogin/RecomDestinations';
import DashBoard from './Components/BackOffice/DashBoard';


class App extends Component {
    render() {
        return (
                <HashRouter>
                    <Switch>
                        <Route exact path = '/' component = {FirstPage}/>
                        <Route exact path = '/Register' component = {RegisterPage}/>
                        <Route exact path = '/ContactUs' component = {ContactPage}/>
                        <Route exact path = '/Profile' component = {ProfilePage}/>
                        <Route exact path = '/About' component = {AboutPage}/>
                        <Route exact path = '/FavTourism' component = {TourOptionsPage}/>
                        <Route exact path = '/Destinations' component = {DestinationsPage}/>
                        <Route exact path = '/CreateRoutes' component = {CreateRoutes}/>
                        <Route exact path = '/ChangePass' component = {ChangePass}/>
                        <Route exact path = '/ChatPage' component = {ChatPage}/>
                        <Route exact path = '/RecomDestinations' component = {RecomDestinations}/>
                        <Route exact path = '/BackOffice'component={DashBoard} />
                        {/* <Route exact path = '/AboutPage' component = {AboutPage}/>*/}
                    </Switch>
                </HashRouter>
        );
    }
}

export default App;
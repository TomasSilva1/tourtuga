// proper initialization
importScripts('https://www.gstatic.com/firebasejs/6.0.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.0.2/firebase-messaging.js');
/*
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('../firebase-messaging-sw.js')
  .then(function(registration) {
    console.log('Registration successful, scope is:', registration.scope);
  }).catch(function(err) {
    console.log('Service worker registration failed, error:', err);
  });
}
*/
 // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDl5GCkOo277llEO4cAeZBwvUzEweiQtvg",
    authDomain: "tour-tuga.firebaseapp.com",
    databaseURL: "https://tour-tuga.firebaseio.com",
    projectId: "tour-tuga",
    storageBucket: "tour-tuga.appspot.com",
    messagingSenderId: "1021351712886",
    appId: "1:1021351712886:web:d532faa69e7d4af8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});


self.addEventListener('notificationclick', (event) => {
  console.log("hiiiiiiiiiiiiiii");
  if (event.action) {
      clients.openWindow(event.action);
  }
  event.notification.close();
}); 
package pt.unl.fct.di.apdc.cloudfive.Objects;

import java.util.List;

public class FavoriteTypes {
	public String tokenID, email;
	public List<String> favoriteTypes;

	public FavoriteTypes(String tokenID, String email, List<String> favoriteTypes) {
		this.tokenID = tokenID;
		this.email = email;
		this.favoriteTypes = favoriteTypes;
	}

}

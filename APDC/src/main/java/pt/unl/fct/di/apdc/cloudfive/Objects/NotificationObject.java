package pt.unl.fct.di.apdc.cloudfive.Objects;

import org.json.JSONObject;

public class NotificationObject {
	
	public JSONObject obj = new JSONObject();

	public NotificationObject(String deviceToken, String msg, String destEmail,int hour,int minutes,int month, int year, int day) {
		obj.put("to", deviceToken).put("notification", new JSONObject().put("title", destEmail).put("body", msg)).put("data", new JSONObject().put("user", destEmail).put("msg", msg).put("hour",hour).put("minute",minutes).put("month",month).put("year",year).put("day",day));
		System.out.println(obj.toString());
	}
}

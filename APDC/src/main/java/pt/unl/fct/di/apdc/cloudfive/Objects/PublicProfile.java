package pt.unl.fct.di.apdc.cloudfive.Objects;

public class PublicProfile {
	
	public String tokenID,email,destEmail;
	
	public PublicProfile(String email, String tokenID, String destEmail) {
		this.tokenID = tokenID;
		this.email = email;
		this.destEmail = destEmail;
	}
}

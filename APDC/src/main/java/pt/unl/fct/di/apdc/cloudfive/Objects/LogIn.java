package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;



public class LogIn {
	public String email, password, tokenID, deviceToken;
	public boolean photo;

	public LogIn(String email, String password, String deviceToken, boolean photo) {
		this.email = email;
		this.password = password;
		this.deviceToken = deviceToken;
		this.photo = photo;
	}

	public Entity getTokenEntity() {
		AuthToken at = new AuthToken(this.email);
		tokenID = at.tokenID;
		Entity e = at.getEntity();
		e.setProperty("deviceToken", deviceToken);
		return e;
	}
}

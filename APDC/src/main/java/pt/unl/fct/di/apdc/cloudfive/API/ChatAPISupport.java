package pt.unl.fct.di.apdc.cloudfive.API;

import com.google.appengine.api.datastore.*;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pt.unl.fct.di.apdc.cloudfive.Objects.BlockMatch;
import pt.unl.fct.di.apdc.cloudfive.Objects.LogIn;
import pt.unl.fct.di.apdc.cloudfive.Objects.Message;
import pt.unl.fct.di.apdc.cloudfive.Objects.NotificationObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

public class ChatAPISupport {
    private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final int PAGE_SIZE = 20;
    private static final String URL_NOTIFICATION = "https://fcm.googleapis.com/fcm/send";

    @SuppressWarnings("unused")
	protected static ResponseEntity<?> getChatMessages(Message request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
            try {
                Key blockKey = KeyFactory.createKey("Block", request.hashID);
                Entity blocked = datastore.get(blockKey);
                return ResponseEntity.status(HttpStatus.OK).body(response.put("Error", "This conversation is blocked").toString());
            } catch (EntityNotFoundException t) {
                if (checkConversation(request, txn, false)) {
                    FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
                    if (request.offset != null && request.offset != "")
                        fetchOptions.startCursor(Cursor.fromWebSafeString(request.offset + ""));
                    Query q = new Query(request.hashID + "").addSort("time", Query.SortDirection.DESCENDING);
                    response = CommonMethods.queryListWithOffset(q, response, fetchOptions, txn, "msgArray","time","offset");
                    txn.commit();
                    if(response.has("Error"))
                        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
                    else
                        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                } else {
                    txn.rollback();
                    return ResponseEntity.status(HttpStatus.OK).body(response.put("Error", "Conversation Does Not Exist").toString());
                }
            }
        } else {
            txn.rollback();
            LOG.info("Invalid Token");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "InvalidToken").toString());
        }
    }

    /**
     * Add this conversation to the blocked conversations deletes the conversation from both the users list of conversations and from the conversation entity
     *
     * @param request
     * @return
     */
    public static ResponseEntity<?> blockMatch(BlockMatch request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if (CommonMethods.handleToken(request.tokenID, request.email1, txn, response)) {
            datastore.put(txn, request.getBlockEntity());
            deleteConversationFromUser(request.email1, request.hashID, txn);
            deleteConversationFromUser(request.email2, request.hashID, txn);
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", request.tokenID).toString());
        }
        txn.rollback();
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
    }

    @SuppressWarnings("unused")
    protected static ResponseEntity<?> createConversation(Message request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        String name = "";
        if(request.email.equalsIgnoreCase(request.destEmail))
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "Cannot talk with your self!").toString());;
        if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
            try {
                Key chatKey = KeyFactory.createKey("ConvList-" + request.email, request.hashID);
                Entity conversation = datastore.get(chatKey);
                response.put("Error", "Conversation already exists");
                LOG.info("Conversation already exists");
                txn.rollback();
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(response.put("Error", "Conversation already exists").toString());
            } catch (EntityNotFoundException e) {
                try {
                    Key blockKey = KeyFactory.createKey("Block", request.hashID);
                    Entity blocked = datastore.get(blockKey);
                    return ResponseEntity.status(HttpStatus.OK).body(response.put("Error", "This conversation is blocked").toString());
                } catch (EntityNotFoundException t) {
                    try {
                        Key chatKey2 = KeyFactory.createKey("ConvList-" + request.destEmail, request.hashID);
                        Entity conversation2 = datastore.get(chatKey2);
                        response.put("Error", "Conversation already exists");
                        LOG.info("Conversation already exists");
                        txn.rollback();
                        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                                .body(response.put("Error", "Conversation already exists").toString());

                    } catch (EntityNotFoundException e2) {
                        datastore.put(txn, request.getChatEntity(getUserName(request.destEmail, txn)));
                        datastore.put(txn, request.getDestEntity(getUserName(request.email, txn)));
                        txn.commit();
                        return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", request.tokenID).toString());
                    }
                }

            } finally {
                if (txn.isActive())
                    txn.rollback();
            }
        } else {
            txn.rollback();
            LOG.info("Invalid Token");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "InvalidToken").toString());
        }
    }

    protected static ResponseEntity<?> newMsg(Message msg) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        String emailTested = msg.email;
        String name = "";
        boolean flag = false;
        try {
            Key key = KeyFactory.createKey("User", msg.email);
            Entity entity = datastore.get(key);
            name = entity.getProperty("firstname") + " " + entity.getProperty("lastname");
            if (CommonMethods.handleToken(msg.tokenID, msg.email, txn, response)) {
                emailTested = msg.destEmail;
                key = KeyFactory.createKey("User", msg.destEmail);
                entity = datastore.get(key);
                if (checkConversation(msg, txn, true)) {
                    datastore.put(txn, msg.getMessageEntity(name));
                    response.put("tokenID", msg.tokenID);
                    // TODO: send notification
                    flag = true;
                    key = KeyFactory.createKey("Token", msg.destEmail);
                    entity = datastore.get(key);
                    String deviceToken = (String) entity.getProperty("deviceToken");
                    if (deviceToken != null) {
                        try {
                            sendNotification(URL_NOTIFICATION, new NotificationObject(deviceToken,msg.msg,msg.email,msg.hora,msg.minutos,msg.mes,msg.ano,msg.dia).obj);
                        } catch (IOException e) {
                            LOG.info("Notification Error");
                        }
                    }
                    entity.setProperty("newMessage",true);
                    datastore.put(txn,entity);
                    txn.commit();
                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                } else {
                    response.put("Error", "Conversation Doesnt Exist");
                    txn.rollback();
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
                }
            } else {
                txn.rollback();
                LOG.info("Invalid Token");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(response.put("Error", "InvalidToken").toString());
            }
        } catch (EntityNotFoundException e) {
            if(flag) {
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            }
            response.put("Error", emailTested + " Doesnt Exist");
            txn.rollback();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        } finally {
            if (txn.isActive())
                txn.rollback();
        }
    }


//	------------------------------------------------------------------------
//	Private methods


    @SuppressWarnings("finally")
	private static boolean checkConversation(Message msg, Transaction txn, boolean f) {
        boolean flag = true;
        try {
            Key chatListKey = KeyFactory.createKey("ConvList-" + msg.email, msg.hashID + "");
            Entity chatList = datastore.get(chatListKey);
            if (f) {
                chatList = addMessageToConvList(msg, chatList);
                datastore.put(txn, chatList);
            }
            chatListKey = KeyFactory.createKey("ConvList-" + msg.destEmail, msg.hashID + "");
            chatList = datastore.get(chatListKey);
            if (f) {
                chatList = addMessageToConvList(msg, chatList);
                datastore.put(txn, chatList);
            }
        } catch (EntityNotFoundException e) {
            flag = false;
        } finally {
            return flag;
        }
    }

    private static Entity addMessageToConvList(Message msg, Entity chatList) {
        chatList.setProperty("msg", msg.msg);
        chatList.setProperty("user", msg.email);
        chatList.setProperty("time", msg.time);
        chatList.setProperty("year", msg.ano);
        chatList.setProperty("month", msg.mes);
        chatList.setProperty("day", msg.dia);
        chatList.setProperty("hour", msg.hora);
        chatList.setProperty("minute", msg.minutos);
        return chatList;
    }

    @SuppressWarnings("unused")
    private static boolean sendNotification(String urlString, JSONObject body) throws IOException {
        URL url = new URL(urlString);
        InputStream stream = null;
        OutputStream out = null;
        HttpURLConnection connection = null;
        String result = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key=AAAA7c1OWHY:APA91bH2VAYpiKBtaZrou39PlGa2GRFBLonxhT2GUG6E2w4OKy4Tsq3qCWZXKXYxoJKeCzGYQLIzwbUBydFV9eRRLjNmO0BlMPpsepMNOWduNnxEVwi-wEJiLR9uN5X-5ytX02_2_6a8");
            out = new BufferedOutputStream(connection.getOutputStream());
            out.write(body.toString().getBytes());
            out.flush();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpsURLConnection.HTTP_OK)
                throw new IOException("HTTP error code: " + responseCode);
            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream, 1024);
                return true;
            }
        } finally {
            if (out != null)
                out.close();
            if (stream != null)
                stream.close();
            if (connection != null)
                connection.disconnect();
        }
        return false;
    }

    private static String readStream(InputStream stream, int bufferSize) throws IOException {
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(stream, "UTF-8");
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }

    @SuppressWarnings("unused")
    private static JSONObject createNotification(String token, String dest, String msg) {
        JSONObject notification = new JSONObject();
        notification.put("to", token);
        System.out.println(token);
        notification.put("notification", new JSONObject().put("title", dest).put("body", msg));
        notification.put("data", new JSONObject().put("user", dest).put("msg", msg));
        return notification;
    }


    /**
     * Deletes conversation from user list of conversations
     *
     * @param email
     * @param txn
     */
    @SuppressWarnings("unused")
	private static void deleteConversationFromUser(String email, String hashID, Transaction txn) {
        try {
            Key chatKey = KeyFactory.createKey("ConvList-" + email, hashID + "");
            Entity user = datastore.get(txn, chatKey);
            datastore.delete(txn, chatKey);
        } catch (EntityNotFoundException e) {
            // User does Not exist is verified before so no need to treat this catch
        }
    }

    private static String getUserName(String email, Transaction txn) {
        try {
            Key key = KeyFactory.createKey("User", email);
            Entity entity = datastore.get(key);
            return ((String)entity.getProperty("firstname") + " " + (String)entity.getProperty("lastname"));
        } catch (EntityNotFoundException e) {
            return null;
        }
    }
}
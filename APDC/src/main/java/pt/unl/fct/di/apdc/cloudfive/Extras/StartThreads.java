package pt.unl.fct.di.apdc.cloudfive.Extras;

import java.util.Calendar;

public class StartThreads {
    public static void startThread(boolean routes, boolean users, boolean matches, boolean logins ){
        System.out.println("StartThreads");
        Calendar c = Calendar.getInstance();
        String date = c.get(Calendar.YEAR)+""+c.get(Calendar.MONTH);
        String day = c.get(Calendar.DAY_OF_MONTH)+"";
        new StatsThread(routes,users,matches,logins,date,day).run();
    }
}

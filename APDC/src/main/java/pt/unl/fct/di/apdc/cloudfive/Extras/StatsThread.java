package pt.unl.fct.di.apdc.cloudfive.Extras;

import com.google.appengine.api.datastore.*;
import pt.unl.fct.di.apdc.cloudfive.Objects.LogIn;

import java.util.logging.Logger;

public class StatsThread {


    private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    boolean routes, users, matches,logins;
    String date,day;

    public StatsThread(boolean routes, boolean users, boolean matches, boolean logins, String date, String day){
        super();
        this.routes = routes;
        this.users = users;
        this.matches = matches;
        this.logins = logins;
        this.date = date;
        this.day = day;
    }

    public void run(){
        System.out.println("RUN THREAD");
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Entity entity = new Entity(this.date,this.day);
        try {
             entity = datastore.get(KeyFactory.createKey(this.date, this.day));
            if (this.routes)
                entity.setProperty("routes",(long)entity.getProperty("routes")+1);
            if (this.users)
                entity.setProperty("users",(long)entity.getProperty("users")+1);
            if (this.matches)
                entity.setProperty("matches",(long)entity.getProperty("matches")+1);
            if (this.logins)
                entity.setProperty("logins",(long)entity.getProperty("logins")+1);
        }catch(EntityNotFoundException e){
            if (this.routes)
                entity.setProperty("routes",1);
            else
                entity.setProperty("routes",0);
            if (this.users)
                entity.setProperty("users",1);
            else
                entity.setProperty("users",0);
            if (this.matches)
                entity.setProperty("matches",1);
            else
                entity.setProperty("matches",0);
            if (this.logins)
                entity.setProperty("logins",1);
            else
                entity.setProperty("logins",0);
        }finally {
            LOG.info("Updated Stats");
            datastore.put(txn,entity);
            txn.commit();
        }


    }

}

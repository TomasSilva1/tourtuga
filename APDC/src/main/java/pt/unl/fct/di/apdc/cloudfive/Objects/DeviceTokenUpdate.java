package pt.unl.fct.di.apdc.cloudfive.Objects;

public class DeviceTokenUpdate {
    public String email,tokenID, deviceToken;

    public DeviceTokenUpdate(String email, String deviceToken,String tokenID){
        this.deviceToken = deviceToken;
        this.tokenID = tokenID;
        this.email = email;
    }
}

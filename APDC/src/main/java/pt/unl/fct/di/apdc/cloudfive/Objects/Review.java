package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class Review {
    public double score;
    public String comentario,email,tokenID,routeID,name;
    public long time;
    public Review(String email, String tokenID, String routeID, String msg, double score,String name){
        this.email = email;
        this.tokenID = tokenID;
        this.routeID = routeID;
        this.comentario = msg;
        this.score = score;
        this.name = name;
        this.time = System.currentTimeMillis();
    }

    public Entity getCommentEntity(){
        Entity comment = new Entity(this.routeID+"-Comments",this.time);
        comment.setProperty("name", this.name);
        comment.setProperty("email",this.email);
        comment.setProperty("msg",this.comentario);
        return comment;
    }
}

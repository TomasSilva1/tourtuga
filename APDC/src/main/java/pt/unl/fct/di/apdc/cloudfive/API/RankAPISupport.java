package pt.unl.fct.di.apdc.cloudfive.API;

import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;

public class RankAPISupport {

	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	protected static ResponseEntity<?> rankByType(String offset, String type) {
		JSONObject response = new JSONObject();
		Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
		FetchOptions fetch = CommonMethods.setOffset(txn, offset);
		Query q = new Query("Rank").addSort(type, Query.SortDirection.DESCENDING);
		CommonMethods.queryListWithOffset(q, response, fetch, txn, "rank", "email", "offset");
		return ResponseEntity.status(HttpStatus.OK).body(response.toString());
	}

	protected static ResponseEntity<?> getUserLevel(String email) {
		JSONObject response = new JSONObject();
		try {
			Entity entity = datastore.get(KeyFactory.createKey("Rank", email));
			Query q = new Query("Rank").addSort("globalPoints", Query.SortDirection.DESCENDING);
			List<Entity> conv = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
			int i = 1;
			for(Entity e : conv) {
				if(e.getKey().getName().equalsIgnoreCase(email))
					break;
				i++;
			}
			response.put("ranking", i);
			CommonMethods.fromEntityToJson(entity, "email", response);
			response.remove("email");
			return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.put("Error", "User doesnt exist");
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
		}

	}
}

package pt.unl.fct.di.apdc.cloudfive.API;

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pt.unl.fct.di.apdc.cloudfive.Objects.*;

import java.util.*;
import java.util.logging.Logger;

public class RouteAPISupport {
    private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final int PAGE_SIZE = 10;
    private static final List<String> typeArray = new ArrayList<>(Arrays.asList("Adventure", "Art", "City", "Culinary", "Music", "Rural", "Cultural", "Eco/Geo", "Nautical", "Religious"));


    /**
     * Checks if a route exixts from that user and with the same title, otherwise creates a new route
     *
     * @param request
     * @return 200 ok in case of success and 406 in case of error
     */
    @SuppressWarnings("unused")
    public static ResponseEntity<?> createRoute(Percurso request) {
        JSONObject response = new JSONObject();
        boolean flag = false;
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key;
        try {
            Entity entity;
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
                flag = true;
                key = KeyFactory.createKey("Route", request.id);
                entity = datastore.get(key);
                txn.rollback();
                LOG.info("Route already exists");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Route already exists").toString());
            } else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
            }

        } catch (EntityNotFoundException e) {
            if (!flag) {
                txn.rollback();
                LOG.info("Invalid Token");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Invalid Token").toString());
            } else {
                List<String> l = checkMarkers(request.markers, txn);
                datastore.put(txn, request.toEntity(l));
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            }
        }
    }

    public static ResponseEntity<?> getRoute(String routeID) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key routeKey = KeyFactory.createKey("Route", routeID);
        try {
            Entity route = datastore.get(routeKey);
            CommonMethods.fromEntityToJson(route, routeID, response);
            FetchOptions fetchOptions = CommonMethods.setOffset(txn,"");
            Query q = new Query(routeID+"-Photo");
            CommonMethods.queryListWithOffset(q,response,fetchOptions,txn,"photos","photo","offset");
            makeRouteJSON(route, response);
            response.put("matchList", getListOfMatchers(txn, routeID));
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        } catch (EntityNotFoundException e) {
            txn.rollback();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Route Doesnt Exist").toString());
        }
    }

    public static ResponseEntity<?> getFirstMarker() {
        JSONObject response = new JSONObject();
        Query q = new Query("Route");
        Iterator<Entity> it = datastore.prepare(q).asIterable().iterator();
        JSONArray arr = new JSONArray();
        Entity entity;
        JSONObject j;
        while (it.hasNext()) {
            entity = it.next();
            j = new JSONObject();
            j.put("routeID", entity.getKey().getName());
            j.put("titulo", entity.getProperty("titulo"));
            EmbeddedEntity first = (EmbeddedEntity) entity.getProperty("first");
            j.put("marker", new JSONObject().put("lat", first.getProperty("lat")).put("lg", first.getProperty("lg")));
            arr.put(j);
        }
        response.put("routes", arr);
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }

    @SuppressWarnings("unchecked")
	public static ResponseEntity<?> getAll() {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Query q = new Query("Route");
        Iterator<Entity> it = datastore.prepare(q).asIterable().iterator();
        JSONArray arr = new JSONArray();
        Entity entity;
        JSONObject j ;
        while (it.hasNext()) {
            j =  new JSONObject();
            entity = it.next();
            CommonMethods.fromEntityToJson(entity, "id",j);
            makeRouteJSON(entity, j);
            if(j.has("imagens")){
                if(j.get("imagens")!=null) {
                    if (((ArrayList<String>) j.get("imagens")).size() > 0) {
                        j.put("imagem", ((ArrayList<String>) j.get("imagens")).get(0));
                    } else
                        j.put("imagem", "");
                }else
                    j.put("imagem","");
                j.remove("imagens");
            }else
                j.put("imagem","");
            arr.put(j);
        }
        response.put("routes", arr);
        txn.commit();
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }

    public static ResponseEntity<?> getRecommended(double lat, double lg){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Query q = new Query("Route").setFilter(Query.CompositeFilterOperator.and(new FilterPredicate("lat",FilterOperator.LESS_THAN,lat+0.1),new FilterPredicate("lat",FilterOperator.GREATER_THAN,lat-0.1)));
        CommonMethods.queryRecommended(q, response, txn, "routes", "routeID", "offset",lg);
        if(response.has("Error"))
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        else
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }

    public static ResponseEntity<?> getAll(String offset1,String offset2,String offset3, String type){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        FetchOptions fetchOptions = CommonMethods.setOffset(txn,offset1);
        int i = PAGE_SIZE;
        response.put("offset1","");
        response.put("offset2","");
        response.put("offset3","");
        if(type.equalsIgnoreCase("all")){
            Query q = new Query("Route");
            response = CommonMethods.queryListWithOffset(q, response, fetchOptions, txn, "routes","routeID","offset1");
            txn.commit();
            if(response.has("Error"))
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
            else {
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            }
        }else if(typeArray.contains(type)){
            if(offset1 != "null") {
                Query q1 = new Query("Route").setFilter(new FilterPredicate("type1", FilterOperator.EQUAL, type));
                response = CommonMethods.queryListWithOffset(q1, response, fetchOptions, txn, "routes", "routeID", "offset1");
                i -= response.getJSONArray("routes").length();
            }else
                response.put("offset1","null");
            if(offset2 != "null") {
                if (i > 0) {
                    response.put("offset1","null");
                    fetchOptions = CommonMethods.setOffset(txn, offset2);
                    Query q2 = new Query("Route").setFilter(new FilterPredicate("type2", FilterOperator.EQUAL, type));
                    response = CommonMethods.queryListWithOffset(q2, response, fetchOptions, txn, "routes", "routeID", "offset2");
                    i -= response.getJSONArray("routes").length();
                }
            }else
                response.put("offset2","null");
            if(offset3!= "null") {
                if(i>0) {
                    response.put("offset2", "null");
                    Query q3 = new Query("Route").setFilter(new FilterPredicate("type3", FilterOperator.EQUAL, type));
                    response = CommonMethods.queryListWithOffset(q3, response, fetchOptions, txn, "routes", "routeID", "offset3");
                }
            }
            txn.commit();
            if(response.has("Error"))
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
            else
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }else{
            Query q = new Query("Route").setFilter(new FilterPredicate("criador",FilterOperator.EQUAL,type));
            response = CommonMethods.queryListWithOffset(q, response, fetchOptions, txn, "routes","routeID","offset");
            txn.commit();
            if(response.has("Error"))
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
            else
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }
    }

    public static ResponseEntity<?> getInterestedRoutes(String email, String tokenID){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if(CommonMethods.handleToken(tokenID,email,txn,response)){
            Query q = new Query("Matches").setFilter(new FilterPredicate("email", FilterOperator.EQUAL, email));
            CommonMethods.queryListWithOffset(q,response,null,txn,"routes","matchID","");
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }else {
            LOG.info("Invalid Token from " + email);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Invalid token").toString());
        }
    }

    public static ResponseEntity<?> getComments(String routeID, String offset) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        FetchOptions fetchOptions = CommonMethods.setOffset(txn,offset);
        Query q = new Query(routeID + "-Comments");
        CommonMethods.queryListWithOffset(q, response, fetchOptions, txn, "comments","routeID","offset");
        txn.commit();
        if(response.has("Error"))
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        else
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }

    public static ResponseEntity<?> comment(CommentRoute request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key = KeyFactory.createKey("Route", request.routeID);
        boolean flag = true;
        try {
            Entity entity = datastore.get(key);
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
                key = KeyFactory.createKey("User", request.email);
                flag = false;
                entity = datastore.get(key);
                datastore.put(txn, request.getEntity(entity.getProperty("firstname") + " " + entity.getProperty("lastname")));
                txn.commit();
                LOG.info("ROUTE COMMENTED");
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            } else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
            }
        } catch (EntityNotFoundException e) {
            txn.rollback();
            if (flag) {
                response.put("Error", "Route not found");
                LOG.info("ROUTE NOT FOUND");
            } else {
                response.put("Error", "User not found");
                LOG.info("USER NOT FOUND");
            }
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        }
    }

    @SuppressWarnings("finally")
	public static ResponseEntity<?> wantAFriend(WantAFriend request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key;
        Entity route = null;
        boolean flag = false;
        try {
            Entity entity;
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
                route = datastore.get(KeyFactory.createKey("Route", request.routeID));
                flag = true;
                key = KeyFactory.createKey("Matches", request.id);
                entity = datastore.get(key);
                if (!checkDate(CommonMethods.fromEntityToJson(entity, "id", new JSONObject()))) {
                    datastore.delete(txn, key);
                    datastore.put(txn, request.getEntity(route));
                    txn.commit();
                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                } else {
                    txn.rollback();
                    LOG.info("Already looking for matches!");
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Already looking for friends in this Route!").toString());
                }
            }else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Invalid token").toString());
            }
        } catch (EntityNotFoundException e) {
            if (flag)
                datastore.put(txn, request.getEntity(route));
            else
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Route does not exist").toString());
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }
    }

	@SuppressWarnings("unchecked")
	public static ResponseEntity<?> getRouteSmall(String routeID) {
        JSONObject response = new JSONObject();
        JSONArray arr = new JSONArray();
        try {
            Entity entity = datastore.get(KeyFactory.createKey("Route", routeID));
            response.put("routeID", routeID);
            response.put("titulo", entity.getProperty("titulo"));
            response.put("distancia", entity.getProperty("distancia"));
            response.put("time", entity.getProperty("time"));
            response.put("markers","");
            ArrayList<String> l = (ArrayList<String>) entity.getProperty("topicos");
            for(String s: l)
            	arr.put(s);
            response.put("favs", arr);
            makeRouteJSON(entity,response);
            response.put("dificuldade", entity.getProperty("dificuldade"));
            if (entity.getProperty("imagens")!=null && ((ArrayList<String>) entity.getProperty("imagens")).size() > 0)
                response.put("imagem", ((ArrayList<String>) entity.getProperty("imagens")).get(0));
            else
                response.put("imagem", "");
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Route not found").toString());
        }
    }

    public static ResponseEntity<?> finishRoute(FinishRoute request){
        JSONObject response = new JSONObject();
        long pL = 0;
        long gL = 0;
        boolean isAdmin = request.isAdmin;
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        try {
            Entity entity;
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {        
                entity = datastore.get(KeyFactory.createKey("Rank",request.email));
                gL = (long) entity.getProperty("globalPoints");
                pL = (long) entity.getProperty("points");
                int p = (int) pL;
                int gP = (int) gL;
                int pointsToGive;
                if(request.distance < 100)
            		pointsToGive =  (int) (50);
            	else
            		pointsToGive =  (int) (50*Math.floor(request.distance/100));
                if(!request.finished) {
                	pointsToGive = (int)(pointsToGive/2);
                	if(isAdmin)
                		pointsToGive = pointsToGive*2;
                	entity.setProperty("globalPoints", (long) gP+pointsToGive);
                	entity.setProperty("points", (long) p+pointsToGive);
                	handlePoints(entity);
                    datastore.put(entity);
                    txn.commit();
                   
                    return ResponseEntity.status(HttpStatus.OK).body(response.put("Note", "Route not completed").toString());
                }
                if(updateListOfRoutesMade(request,txn)) {
                	if(isAdmin)
                		pointsToGive = pointsToGive*2;
                    entity.setProperty("points",(long) p+pointsToGive);
                    entity.setProperty("globalPoints", (long) gP+pointsToGive);
                    handlePoints(entity);
                    datastore.put(entity);
                    txn.commit();
                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                }else{
                	pointsToGive = (int)pointsToGive/2;
                	if(isAdmin)
                		pointsToGive = pointsToGive*2;
                	entity.setProperty("globalPoints", (long) gP+pointsToGive);
                    entity.setProperty("points",(long) p+pointsToGive);
                    handlePoints(entity);
                    datastore.put(entity);
                    txn.commit();
                    return ResponseEntity.status(HttpStatus.OK).body(response.put("Note", "Route Already Completed").toString());
                }
            } else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
            }
        }catch(EntityNotFoundException e){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "User Does Not Exist!").toString());
        }
    }

    public static ResponseEntity<?> addPhoto(AddPhoto request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        try {
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
                datastore.get(KeyFactory.createKey("Route",request.routeID));
                if(request.photo!=null && request.photo !="") {
                    datastore.put(txn, request.getEntity());
                    txn.commit();
                }
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            } else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
            }
        }catch(EntityNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Route Does Not Exist!").toString());
        }
    }

    public static ResponseEntity<?> getCompletedRoutes(RequestWithToken request){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if(CommonMethods.handleToken(request.tokenID,request.email, txn, response)) {
            Query q = new Query(request.email + "-Done");
            Iterator<Entity> it = datastore.prepare(q).asIterable().iterator();
            JSONArray arr = new JSONArray();
            Entity entity;
            JSONObject j;
            while (it.hasNext()) {
                j = new JSONObject();
                entity = it.next();
                CommonMethods.fromEntityToJson(entity, "routeID",j);
                makeRouteJSON(entity, j);
                arr.put(j);
            }
            response.put("routes", arr);
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }else {
            LOG.info("Invalid Token from " + request.email);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
        }
    }

    @SuppressWarnings("unused")
	public static ResponseEntity<?> removeFromMatchList(WantAFriend request){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if(CommonMethods.handleToken(request.tokenID,request.email,txn,response)){
            Entity entity;
            Key key = KeyFactory.createKey("Matches",request.id);
            try{
                entity = datastore.get(key);
                datastore.delete(txn,key);
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            }catch(EntityNotFoundException e){
                return ResponseEntity.status(HttpStatus.OK).body(response.put("Error","This user is not in the match list").toString());
            }
        }else {
            LOG.info("Invalid Token from " + request.email);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
        }
    }

    public static ResponseEntity<?> reviewRoute(Review request){
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if(CommonMethods.handleToken(request.tokenID,request.email,txn,response)){
            try {
                Entity entity = datastore.get(KeyFactory.createKey("Route", request.routeID));
                Entity rankEntity = datastore.get(KeyFactory.createKey("Rank", (String)entity.getProperty("criador")));
                long cP = (Long) rankEntity.getProperty("creatorPoints");
                long gP = (Long) rankEntity.getProperty("globalPoints");
                int p = (int) cP;
                int g = (int) gP;
                int pointsToGive = (int) (request.score*10);
                rankEntity.setProperty("creatorPoints", (long) p+pointsToGive);
                rankEntity.setProperty("globalPoints", (long) g+pointsToGive);
                entity.setProperty("score",(double)entity.getProperty("score")+request.score);
                entity.setProperty("reviews",(long)entity.getProperty("reviews")+1);
                handlePoints(rankEntity);
                datastore.put(rankEntity);
                datastore.put(entity);
                if(request.comentario!=null)
                    datastore.put(request.getCommentEntity());
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            }catch(EntityNotFoundException e){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Route not found").toString());
            }
        }else {
            LOG.info("Invalid Token from " + request.email);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new JSONObject().put("Error", "Invalid token").toString());
        }
    }
    
    public static ResponseEntity<?> getRoutesSearch(){
        JSONObject response = new JSONObject();
        JSONArray array = new JSONArray();
        Query q = new Query("RouteSearch");
        q.setKeysOnly();
        List<Entity> keys = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
        for(Entity e: keys) {
            System.out.println(e.toString());
            array.put(e.getKey().getName());
        }
        response.put("search", array);
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        
    }
    //------------------Private Methods-----------------------------

    private static List<String> checkMarkers(List<Markers> markers, Transaction txn) {
        List<String> list = new ArrayList<>();
        for (Markers m : markers) {
            checkMarker(m, txn);
            list.add(m.id);
        }
        return list;
    }

    private static void checkMarker(Markers marker, Transaction txn) {
        Key key = KeyFactory.createKey("Marker", marker.id);
        try {
            datastore.get(key);
        } catch (EntityNotFoundException e) {
            datastore.put(txn, marker.getEntity());
        }
    }

    @SuppressWarnings("unchecked")
	public static JSONObject makeRouteJSON(Entity e, JSONObject j) {
        JSONArray arr = new JSONArray();
        j.remove("markers");
        List<String> markersS = (ArrayList<String>) e.getProperty("markers");
        List<Key> markerKeys = new ArrayList<Key>();
        if(!markersS.isEmpty()) {
            for (String s : markersS)
                markerKeys.add(KeyFactory.createKey("Marker", s));
            Filter filt = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.IN, markerKeys);
            Query query = new Query("Marker").setFilter(filt);
            List<Entity> conv = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
            for (Entity ent : conv)
                arr.put(CommonMethods.fromEntityToJson(ent, "hash", new JSONObject()));
        }
        j.put("markers", arr);
        return j;
    }

    private static boolean checkDate(JSONObject friend) {
        Calendar c = Calendar.getInstance();
        System.out.println(c.get(Calendar.HOUR_OF_DAY));
        if ((long) friend.get("year") >= c.get(Calendar.YEAR))
            if ((long) friend.get("year") > c.get(Calendar.YEAR))
                return true;
            else if ((long) friend.get("month") >= c.get(Calendar.MONTH))
                if ((long) friend.get("month") > c.get(Calendar.MONTH))
                    return true;
                else if ((long) friend.get("day") >= c.get(Calendar.DAY_OF_MONTH))
                    if ((long) friend.get("day") > c.get(Calendar.DAY_OF_MONTH))
                        return true;
                    else if ((long) friend.get("hour") >= c.get(Calendar.HOUR_OF_DAY))
                        if ((long) friend.get("hour") > c.get(Calendar.HOUR_OF_DAY))
                            return true;
                        else if ((long) friend.get("minutes") >= c.get(Calendar.MINUTE))
                            if ((long) friend.get("minutes") > c.get(Calendar.MINUTE))
                                return true;
        return false;
    }

    private static JSONArray getListOfMatchers(Transaction txn, String routeID) {
        JSONArray arr = new JSONArray();
        Filter filter = new FilterPredicate("routeID", FilterOperator.EQUAL, routeID);
        Query query = new Query("Matches").setFilter(filter);
        List<Entity> list = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
        System.out.println(list.size());
        JSONObject j ;
        boolean flag = false;
        for (Entity e : list) {
            j = new JSONObject();
            CommonMethods.fromEntityToJson(e, "id",j);
                arr.put(j);
        }
        if (flag)
            txn.commit();
        return arr;
    }

    public static ResponseEntity<?> updateRoute(UpdateRoute request){
        JSONObject response = new JSONObject();
        boolean flag = false;
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key;
        try {
            Entity entity;
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
                entity = datastore.get(KeyFactory.createKey("Route", request.id));
                    List<String> l = checkMarkers(request.markers, txn);
                    datastore.put(txn, request.toEntity(l,entity));
                    datastore.put(txn,request.getPhotoEntity());
                    txn.commit();
                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            } else {
                LOG.info("Invalid Token from " + request.email);
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Invalid token").toString());
            }

        } catch (EntityNotFoundException e) {
            LOG.info("Route Does Not Exist!" + request.email);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.put("Error", "Route Does Not Exist!").toString());
        }
    }

    @SuppressWarnings("unused")
	private static boolean updateListOfRoutesMade(FinishRoute request,Transaction txn) throws EntityNotFoundException{
        try{
            Entity entity = datastore.get(KeyFactory.createKey(request.email+"-Done",request.routeID));
            return false;
        }catch(EntityNotFoundException e){
        	Entity routeEntity = datastore.get(KeyFactory.createKey("Route", request.routeID));
        	Entity entity = request.getEntity();
        	for (Map.Entry<String, Object> entry : routeEntity.getProperties().entrySet()) {
        		entity.setProperty(entry.getKey(), entry.getValue());
            }
        	
            datastore.put(entity);
            return true;
        }
    }
    
    private static void handlePoints(Entity entity) {
        long pointsL = (long) entity.getProperty("globalPoints");
        int points = (int) pointsL;
        int level = 1;
        int n1 = 100;
        int n2 = 200;
        boolean done = false;
        if(points<n1) {
        	done = true;
        }
        while(!done) {
        	level++;
        	if(points<n2)
        		done=true;
        	int tn1 = n1;
        	int tn2 = n2;
        	n1 = tn2;
        	n2 = tn1+n2;
        }
        entity.setProperty("level", (long)level);
        entity.setProperty("bar", (long)n1);
    }
}

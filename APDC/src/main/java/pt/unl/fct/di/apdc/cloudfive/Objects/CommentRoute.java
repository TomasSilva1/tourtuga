package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class CommentRoute {

    public String email,msg,routeID,tokenID;
    public long time;

    public CommentRoute(String email, String msg, String routeID, String tokenID){
        this.email = email;
        this.msg = msg;
        this.routeID = routeID;
        this.tokenID = tokenID;
        this.time = System.currentTimeMillis();
    }

    public Entity getEntity(String name){
        Entity comment = new Entity(this.routeID+"-Comments",this.time);
        comment.setProperty("name", name);
        comment.setProperty("email",this.email);
        comment.setProperty("msg",this.msg);
        return comment;
    }
}

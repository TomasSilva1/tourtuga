package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;

public class UpdateUser {

	public String firstname, lastname, email, password, photo, privado, telemovel, morada, tokenID, oldEmail,age,cc;
	public int role;
	public List<String> favoriteTypes;

	public UpdateUser(String firstname, String lastname, String email, String password, String photo, String privado,
			String age, String telemovel, String morada, String tokenID, String oldEmail, List<String> favTourism, String cc) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.photo = photo;
		this.privado = privado;
		this.age = age;
		this.cc = cc;
		this.telemovel = telemovel;
		this.morada = morada;
		this.tokenID = tokenID;
		this.role = 0;
		this.favoriteTypes = favTourism;
		if (oldEmail != null)
			this.oldEmail = oldEmail;
		else
			this.oldEmail = email;

	}

	public Entity getEntity(Entity user) {
		Entity u;
		if (this.oldEmail != this.email) {
			u = new Entity("User", this.email);
			if (this.firstname != null)
				u.setProperty("firstname", this.firstname);
			else
				u.setProperty("firstname", user.getProperty("firstname"));
			if (this.lastname != null)
				u.setProperty("lastname", this.lastname);
			else
				u.setProperty("lastname", user.getProperty("lastname"));
			if (this.password != null && !this.password.equals(""))
				u.setProperty("password", DigestUtils.sha512Hex(this.password));
			else
				u.setProperty("password", user.getProperty("password"));
			if (this.photo != null)
				u.setProperty("photo", this.photo);
			else
				u.setProperty("photo", user.getProperty("photo"));
			if (this.privado != null)
				u.setProperty("privado", this.privado);
			else
				u.setProperty("privado", user.getProperty("privado"));
			if (this.age != null)
				u.setProperty("age", this.age);
			else
				u.setProperty("age", user.getProperty("age"));
			if (this.cc != null)
				u.setProperty("cc", this.cc);
			else
				u.setProperty("cc", user.getProperty("cc"));
			if (this.telemovel != null)
				u.setProperty("telemovel", this.telemovel);
			else
				u.setProperty("telemovel", user.getProperty("telemovel"));
			if (this.morada != null)
				u.setProperty("morada", this.morada);
			else
				u.setProperty("morada", user.getProperty("morada"));
			if (this.role != 0)
				u.setProperty("role", this.role);
			else
				u.setProperty("role", user.getProperty("role"));
			if (this.favoriteTypes != null)
				u.setProperty("favTourism", this.favoriteTypes);
			else
				u.setProperty("favTourism", user.getProperty("favTourism"));
			return u;
		}
		if (this.firstname != null)
			user.setProperty("firstname", this.firstname);
		if (this.lastname != null)
			user.setProperty("lastname", this.lastname);
		if (this.password != null && !this.password.equals(""))
			user.setProperty("password", DigestUtils.sha512Hex(this.password));
		if (this.photo != null)
			user.setProperty("photo", this.photo);
		if (this.privado != null)
			user.setProperty("privado", this.privado);
		if (this.age != null)
			user.setProperty("age", this.age);
		if (this.cc != null)
			user.setProperty("cc", this.cc);
		if (this.telemovel != null)
			user.setProperty("telemovel", this.telemovel);
		if (this.morada != null)
			user.setProperty("morada", this.morada);
		if (this.role != 0)
			user.setProperty("role", this.role);
		if (this.favoriteTypes != null)
			user.setProperty("favTourism", this.favoriteTypes);
		return user;
	}

	public Entity getTokenEntity() {
		AuthToken t = new AuthToken(this.email);
		this.tokenID = t.tokenID;
		return t.getEntity();
	}

}

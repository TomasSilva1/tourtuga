package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.EmbeddedEntity;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.GeoPt;
import org.json.JSONObject;

import java.util.List;

public class UpdateRoute {
    public String titulo, descricao, email, tokenID, id,photo;
    public int dificuldade, time;
    public float distancia;
    public List<String> topicos, imagens;
    public List<Markers> markers;
    public long points;

    public UpdateRoute(String titulo, String descricao, int dificuldade, float distancia, List<String> topicos, List<Markers> markers, List<String> imagens, String email, String tokenID, int time, String photo) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dificuldade = dificuldade;
        this.distancia = distancia;
        this.topicos = topicos;
        this.markers = markers;
        this.imagens = imagens;
        this.email = email;
        this.tokenID = tokenID;
        this.time = time;
        this.photo = photo;
        this.id = (this.titulo.trim() + this.email.trim()).hashCode() + "";
        this.points = Math.round((distancia/150)*dificuldade);
    }

    public String toString() {
        return new JSONObject().put("titulo", this.titulo).put("descricao", this.descricao).put("dificuldade", this.dificuldade).put("distancia", this.distancia).put("topicos", this.topicos.toString()).put("markers", this.markers.toString()).put("imagens", this.imagens.toString()).put("criador", this.email).toString();
    }

    public Entity getPhotoEntity(){
        return new AddPhoto(this.email,this.tokenID,this.id,this.photo).getEntity();
    }

    public Entity toEntity(List<String> l,Entity e) {
        Entity percurso = new Entity("Route", this.id);
        percurso.setProperty("titulo",(this.titulo!=null?this.titulo:e.getProperty("titulo")));
        percurso.setProperty("descricao",(this.descricao!=null?this.descricao:e.getProperty("descricao")));
        percurso.setProperty("dificuldade",(this.dificuldade!=0?this.dificuldade:e.getProperty("dificuldade")));
        percurso.setProperty("distancia",(this.distancia!=0?this.distancia:e.getProperty("distancia")));
        percurso.setProperty("topicos",(this.topicos!=null?this.topicos:e.getProperty("topicos")));
        if(this.topicos.isEmpty()) {
            percurso.setProperty("type1", null);
            percurso.setProperty("type2",null);
            percurso.setProperty("type3",null);
        }else{
            percurso.setProperty("type1",this.topicos.get(0));
            if(this.topicos.size()>1){
                percurso.setProperty("type2",this.topicos.get(1));
                if(this.topicos.size() > 2)
                    percurso.setProperty("type3",this.topicos.get(2));
                else
                    percurso.setProperty("type3",null);
            }else {
                percurso.setProperty("type2", null);
                percurso.setProperty("type3",null);
            }
        }
        percurso.setProperty("markers", l);
        percurso.setProperty("imagens",(this.imagens!=null?this.imagens:e.getProperty("imagens")));
        percurso.setProperty("criador",(this.email!=null?this.email:e.getProperty("criador")));
        percurso.setProperty("time",(this.time!=0?this.time:e.getProperty("time")));
        percurso.setProperty("points",(this.points!=0?this.points:e.getProperty("points")));
        percurso.setProperty("score",e.getProperty("score"));
        percurso.setProperty("reviews",e.getProperty("reviews"));
        percurso.setProperty("lat",markers.get(0).lat);
        percurso.setProperty("lg",markers.get(0).lg);
        if (!markers.isEmpty()) {
            EmbeddedEntity firstMarker = new EmbeddedEntity();
            firstMarker.setProperty("lat", markers.get(0).lat);
            firstMarker.setProperty("lg", markers.get(0).lg);
            percurso.setProperty("first", firstMarker);
        }
        return percurso;
    }
}

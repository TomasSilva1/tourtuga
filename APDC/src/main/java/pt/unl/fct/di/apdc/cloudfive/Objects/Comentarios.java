package pt.unl.fct.di.apdc.cloudfive.Objects;

import org.json.JSONObject;

public class Comentarios {
    String name, email, msg;

    public Comentarios(String name, String email, String msg) {
        this.name = name;
        this.email = email;
        this.msg = msg;
    }

    public String toString() {
        return new JSONObject().put("name", this.name).put("email", this.email).put("msg", msg).toString();
    }
}

package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;
import org.json.JSONObject;

public class Markers {

    public String titulo,photo,descricao;
    public double lat, lg;
    public String id;

    public Markers(double lat, double lg,String titulo, String photo, String descricao) {
        this.lat = lat;
        this.lg = lg;
        this.id = lat + "" + lg;
        this.titulo=titulo;
        this.photo = photo;
        this.descricao = descricao;
    }

    public String toString() {
        return new JSONObject().put("lat", this.lat).put("lg", this.lg).toString();
    }

    public Entity getEntity() {
        Entity marker = new Entity("Marker", this.id);
        marker.setProperty("lat", this.lat);
        marker.setProperty("lg", this.lg);
        marker.setProperty("titulo",this.titulo);
        marker.setProperty("photo", this.photo);
        marker.setProperty("descricao",descricao);
        return marker;
    }
}

package pt.unl.fct.di.apdc.cloudfive.API;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;

public class NewsAPISupport {
private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    
    
    protected static ResponseEntity<?> shortNews() {
		JSONObject response = new JSONObject();
		JSONObject temp;
		JSONArray newsA = new JSONArray();
		Query q = new Query("News");
		List<Entity> news = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
         for (Entity e : news) {
        	 temp= new JSONObject();
        	 CommonMethods.fromEntityToJson(e, "newID", temp);
        	 temp.remove("body");
        	 temp.remove("intro");
        	 newsA.put(temp);
         }
             
         response.put("news", newsA);
         return ResponseEntity.status(HttpStatus.OK)
                 .body(response.toString());
    }
    
    protected static ResponseEntity<?> getNew(String newID) {
		JSONObject response = new JSONObject();
		Entity newEntity;
		try {
			newEntity = datastore.get(KeyFactory.createKey("News", newID));
			CommonMethods.fromEntityToJson(newEntity, "newID", response);
	        return ResponseEntity.status(HttpStatus.OK)
	                 .body(response.toString());
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
	                 .body("New doesnt exist");
		}
    }
    
    protected static ResponseEntity<?> cloneRoutes() {
		Query q = new Query("Route");
		Entity toCreate;
		String key;
		List<Entity> routes = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
		for(Entity e: routes) {
			key = (String) e.getProperty("titulo")+"&/!"+e.getProperty("criador");
			toCreate = new Entity("RouteSearch", key);
			datastore.put(toCreate);
		}
		 return ResponseEntity.status(HttpStatus.OK)
                 .body("all good");
    }
    
}

package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class BlockMatch {
	public String email1, email2, tokenID,hashID;
	
	public BlockMatch(String email1, String email2, String tokenID) {
		this.email1 = email1;
		this.email2 = email2;
		this.tokenID = tokenID;
		if (email1.compareTo(email2) < 0)
			this.hashID = (email1 + email2).hashCode()+"";
		else
			this.hashID = (email2 + email2).hashCode()+"";
	}
	
	public Entity getBlockEntity() {
		Entity block = new Entity("Block", this.hashID);
		return block;
	}
}

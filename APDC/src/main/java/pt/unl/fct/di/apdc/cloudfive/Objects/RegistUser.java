package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;

public class RegistUser {

	public String firstname, lastname, email, password, photo, privado, telemovel, morada, tokenID, oldEmail, age, cc, deviceToken;
	public int role;
	public boolean newPass;

	public RegistUser(String firstname, String lastname, String email, String password, String photo, String privado, String age,
			String telemovel, String morada, String tokenID, String oldEmail, String cc, String deviceToken) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.photo = (photo!=null?photo:"https://tour-tuga.appspot.com/static/media/avatar.f2494f16.png");
		this.privado = privado;
		this.telemovel = telemovel;
		this.age = age;
		this.morada = morada;
		this.tokenID = tokenID;
		this.role = 0;
		this.cc = cc;
		this.deviceToken = deviceToken;
		if(oldEmail!=null)
			this.oldEmail = oldEmail;
		else
			this.oldEmail = email;
		if(this.password != null)
			this.newPass = this.password.equals("");
		else {
			this.newPass = false;
			
		}
		}

	public void updatePass(String password) {
		this.password = password;
	}

	public Entity getEntity() {
		Entity user = new Entity("User", this.email);
		user.setProperty("firstname", this.firstname);
		user.setProperty("lastname", this.lastname);
		if (!this.newPass)
			user.setProperty("password", DigestUtils.sha512Hex(this.password));
		else
			user.setProperty("password", this.password);
		user.setProperty("photo", this.photo);
		user.setProperty("privado", this.privado);
		user.setProperty("age", this.age);
		user.setProperty("telemovel", this.telemovel);
		user.setProperty("morada", this.morada);
		user.setProperty("role", this.role);
		user.setProperty("cc", this.cc);
		user.setProperty("conversations", new ArrayList<String>());
		user.setProperty("premium", false);
		return user;
	}

	public Entity getPointsEntity() {
		Entity rank = new Entity("Rank", this.email);
		rank.setIndexedProperty("firstName", this.firstname);
		rank.setIndexedProperty("lastName", this.lastname);
		rank.setProperty("points", 0);
		rank.setProperty("level", 1);
		rank.setProperty("bar", 100);
		rank.setProperty("creatorPoints", 0);
		rank.setProperty("globalPoints", 0);
		return rank;
	}
	public Entity getTokenEntity() {
		AuthToken t = new AuthToken(this.email);
		this.tokenID = t.tokenID;
		Entity e = t.getEntity();
		e.setProperty("deviceToken", deviceToken);
		return e;
	}
}

package pt.unl.fct.di.apdc.cloudfive.Objects;

public class RequestWithToken {

	public String tokenID, email;

	public RequestWithToken(String email, String tokenID) {
		this.email = email;
		this.tokenID = tokenID;
	}
}

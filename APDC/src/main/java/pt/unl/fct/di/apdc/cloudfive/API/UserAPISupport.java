package pt.unl.fct.di.apdc.cloudfive.API;

import com.google.appengine.api.datastore.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pt.unl.fct.di.apdc.cloudfive.Objects.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UserAPISupport {
    private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    // private static final String URL_NOTIFICATION =
    // "https://fcm.googleapis.com/fcm/send";

    /**
     * if email and password are correct creates a token, stores the token in the
     * database and returns the token in the Response
     *
     * @param i
     * @return 200 ok if email and password are correct, 406 if not
     */
    @SuppressWarnings("unchecked")
	protected static ResponseEntity<?> logIn(LogIn i) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        try {
            Key userKey = KeyFactory.createKey("User", i.email);
            Entity user = datastore.get(userKey);
            if (user.getProperty("password").equals(DigestUtils.sha512Hex(i.password))) {
                datastore.put(txn, i.getTokenEntity());
                response.put("firstname", user.getProperty("firstname"));
                response.put("lastname", user.getProperty("lastname"));
                
                List<String> favTourisms = (ArrayList<String>) user.getProperty("favTourism");
                JSONArray arr = new JSONArray(favTourisms);
                response.put("tourisms", arr);
                if((long)user.getProperty("role") == 1)
                	response.put("role", 1);
                else {
                	response.put("role", 0);
                	Entity userRank = datastore.get(KeyFactory.createKey("Rank", i.email));
                    response.put("level", userRank.getProperty("level"));
                    response.put("points", userRank.getProperty("points"));
                    response.put("bar", userRank.getProperty("bar"));
                }
                if (i.photo)
                    response.put("photo", user.getProperty("photo"));
                response.put("premium", user.getProperty("premium"));
                LOG.info("User " + i.email + " loged in");
                
                
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", i.tokenID).toString());
            } else {
                response.put("Error", "Wrong Password");
                LOG.info("Wrong Password");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
            }
        } catch (EntityNotFoundException e) {
            txn.rollback();
            response.put("Error", "Username Doesnt Exist");
            LOG.info("Username Doesnt Exist");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        } finally {
            if (txn.isActive())
                txn.rollback();
        }
    }

    /**
     * if user doesnt exist creates the user with the given data, stores the user
     * and the respectove token in the database and returns the tokenID in the
     * Response
     *
     * @param r
     * @return 200 Ok if user doesnt exist alrdy, 406 if it does
     */
    @SuppressWarnings("unused")
    protected static ResponseEntity<?> registUser(RegistUser r) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        try {
            Key userKey = KeyFactory.createKey("User", r.email);
            Entity user = datastore.get(userKey);
            response.put("Error", "User already exists");
            txn.rollback();
            LOG.info("User Already Exists");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        } catch (EntityNotFoundException e) {
            datastore.put(txn, r.getEntity());
            datastore.put(txn, r.getTokenEntity());
            datastore.put(txn, r.getPointsEntity());
            txn.commit();

            response.put("firstname", r.firstname);
            response.put("lastname", r.lastname);
            response.put("tokenID", r.tokenID);
            LOG.info("User: " + r.email + " registered");
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        } finally {
            if (txn.isActive())
                txn.rollback();
        }
    }

    protected static ResponseEntity<?> deleteUser(RequestWithToken request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
            Key userKey = KeyFactory.createKey("Token", request.email);
            Key tokenKey = KeyFactory.createKey("User", request.email);
            datastore.delete(txn, userKey);
            datastore.delete(txn, tokenKey);
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body("");
        }
        txn.rollback();
        response.put("Error", "Invalid Token");
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
    }

    /**
     * LogsUser out of session deleting his token
     *
     * @param email
     * @return 200 OK or 406 if user token is invalid
     */
    protected static ResponseEntity<?> logout(String email, String tokenID) {
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        JSONObject response = new JSONObject();
        if (CommonMethods.handleToken(tokenID, email, txn, response)) {
            Key userKey = KeyFactory.createKey("Token", email);
            datastore.delete(userKey);
            txn.commit();
            LOG.info(email + " logout");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "Invalid token").toString());
        }
    }

    /**
     * Updates User info if token and email are confirmed
     *
     * @param userReg
     * @return 200 OK or 406 if user token is invalid
     */
    protected static ResponseEntity<?> updateUser(UpdateUser userReg) {
        Entity user = null;
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        JSONObject response = new JSONObject();
        Key userKey = KeyFactory.createKey("User", userReg.oldEmail);
        try {
            user = datastore.get(userKey);
        } catch (EntityNotFoundException e1) {
            LOG.info("User does not exist");
            txn.rollback();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(new JSONObject().put("Error", "User doesn't exists").toString());
        }
        if (CommonMethods.handleToken(userReg.tokenID, userReg.oldEmail, txn, response)) {
            deleteUser(userReg.oldEmail, userKey, txn);
            if (userReg.oldEmail != userReg.email) {
                deleteToken(userReg.oldEmail, txn);
                AuthToken token = new AuthToken(userReg.email);
                datastore.put(txn, token.getEntity());
            }
            datastore.put(txn, userReg.getEntity(user));
            txn.commit();
            LOG.info("User Updated");
            return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", userReg.tokenID).toString());
        } else {
            LOG.info("Invalid Token");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "Invalid Token").toString());
        }
    }

    public static ResponseEntity<?> updateUserPreferences(FavoriteTypes fav) {
        Entity user;
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        JSONObject response = new JSONObject();
        Key userKey = KeyFactory.createKey("User", fav.email);
        try {
            if (CommonMethods.handleToken(fav.tokenID, fav.email, txn, response)) {
                user = datastore.get(userKey);
                user.setProperty("favTourism", fav.favoriteTypes);
                datastore.put(txn, user);
                txn.commit();
                LOG.info("Updated User Preferences");
                return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", fav.tokenID).toString());
            } else {
                LOG.info("Invalid Token");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(response.put("Error", "Invalid Token").toString());
            }
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.put("Error", "User Not Found").toString());
        }
    }

    /**
     * LogsUser out of session deleting his token
     *
     * @param request
     * @return 200 OK or 406 if user token is invalid
     */
    protected static ResponseEntity<?> getUserInfo(RequestWithToken request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if(request.email==null || request.tokenID==null)
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "InvalidToken").toString());
        try {
            Key userKey = KeyFactory.createKey("User", request.email);
            Entity user = datastore.get(userKey);
            if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
            	
                CommonMethods.fromEntityToJson(user, "email",response);
                response.put("tokenID", request.tokenID);
                txn.commit();
                LOG.info("Get " + request.email + " info");
                return ResponseEntity.status(HttpStatus.OK).body(response.toString());
            } else {
                txn.rollback();
                LOG.info("Invalid Token");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(response.put("Error", "InvalidToken").toString());
            }
        } catch (EntityNotFoundException e) {
            response.put("Error", "User Doesnt Exist");
            txn.rollback();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
        } finally {
            if (txn.isActive())
                txn.rollback();
        }
    }

	/*
	public static ResponseEntity<?> getUserProfile(PublicProfile request) {
		JSONObject response = new JSONObject();
		Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
		String emaiTested = request.email;
		try {
			Key key = KeyFactory.createKey("User", request.email);
			Entity entity = datastore.get(key);
			if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
				response.put("tokenID", request.tokenID);
				emaiTested = request.destEmail;
				key = KeyFactory.createKey("User", request.destEmail);
				entity = datastore.get(key);
				if (((String) entity.getProperty("privado")).equalsIgnoreCase("private")) {
					response.put("Error", "Private Profile");
					return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
				} else {
					JSONObject n = CommonMethods.fromEntityToJson(entity, "email");
					n.put("tokenID", response.get("tokenID"));
					n.put("newMessage", response.get("newMessage"));
					response = n;
					return ResponseEntity.status(HttpStatus.OK).body(response.toString());
				}
			} else {
				return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
						.body(response.put("Error", "InvalidToken").toString());
			}
		} catch (EntityNotFoundException e) {
			response.put("Error", emaiTested + " Doesnt Exist");
			txn.rollback();
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response.toString());
		} finally {
			if (txn.isActive())
				txn.rollback();
		}
	}
*/

    public static ResponseEntity<?> getUserProfile(String email) {
        JSONObject response = new JSONObject();
        try {
            Key key = KeyFactory.createKey("User", email);
            Entity entity = datastore.get(key);
            response.put("photo", entity.getProperty("photo"));
            response.put("email", email);
            response.put("name", entity.getProperty("firstname") + " " + entity.getProperty("lastname"));
            if (!((String) entity.getProperty("privado")).equalsIgnoreCase("private")) {
                //TODO: Lista de percursos
                //TODO: addicionar propriedades que so se podem ver quando o user profile é publico
            }
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.put("Error", "User not found").toString());
        }
    }

    /**
     * Returns list of user conversations or null if no conversation exists
     *
     * @param request
     * @return
     */
    public static ResponseEntity<?> getUserConversations(RequestWithToken request) {
        JSONObject response = new JSONObject();
        JSONArray entities = new JSONArray();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        if (CommonMethods.handleToken(request.tokenID, request.email, txn, response)) {
            Query query = new Query("ConvList-" + request.email).addSort("time", Query.SortDirection.DESCENDING);
            List<Entity> conv = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
            for (Entity e : conv)
                entities.put(CommonMethods.fromEntityToJson(e, "hashID",new JSONObject()));
            response.put("conversations", entities);
            try {
                Entity e = datastore.get(KeyFactory.createKey("Token", request.email));
                e.setProperty("newMessage", false);
                datastore.put(e);
            }catch(EntityNotFoundException e){
                //Never happens
            }
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        } else
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(response.put("Error", "Invalid token").toString());

    }

    /**
     * Updates the user token expiration date if both token and email correspond
     *
     * @param request
     * @return 200 OK in case of success and 406 in case of error
     */
    public static ResponseEntity<?> validateToken(RequestWithToken request) {
        JSONObject response = new JSONObject();
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key = KeyFactory.createKey("Token", request.email);
        try {
            Entity token = datastore.get(key);
            CommonMethods.checkForMessages(token, response);
            System.out.println(response.toString());
            if (token.getProperty("tokenID").equals(request.tokenID)) {
                token.setProperty("expirationData", System.currentTimeMillis() + AuthToken.EXPIRATION_TIME);
                datastore.put(txn, token);
                LOG.info("Token expiration data updated");
                txn.commit();
                return ResponseEntity.status(HttpStatus.OK).body(response.put("tokenID", request.tokenID).toString());
            } else {
                datastore.delete(key);
                txn.commit();
                LOG.info("Invalid Token");
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(new JSONObject().put("Error", "Invalid Token").toString());
            }
        } catch (EntityNotFoundException e) {
            txn.rollback();
            LOG.info("Invalid Token");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(new JSONObject().put("Error", "Invalid Token").toString());
        }
    }

    public static ResponseEntity<?> setDeviceToken(String token, String email) {
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key = KeyFactory.createKey("Token", email);
        try {
            Entity entity = datastore.get(key);
            entity.setProperty("deviceToken", token);
            datastore.put(txn, entity);
            txn.commit();
            return ResponseEntity.status(HttpStatus.OK).body(new JSONObject().toString());
        } catch (EntityNotFoundException e) {
            txn.rollback();
            LOG.info("Invalid Email");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(new JSONObject().put("Error", "Invalid Email").toString());
        }
    }
    
    public static ResponseEntity<?> adminUser(RequestWithToken r) {
        Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withDefaults().setXG(true));
        Key key = KeyFactory.createKey("User", r.email);
        JSONObject j = new JSONObject();
        try {
        	if(CommonMethods.handleToken(r.tokenID, r.email, txn, j)) {
	            Entity entity = datastore.get(key);
	            entity.setProperty("premium", true);
	            datastore.put(txn, entity);
	            txn.commit();
	            return ResponseEntity.status(HttpStatus.OK).body(new JSONObject().toString());
        	}else
        		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                        .body(new JSONObject().put("Error", "Invalid Token").toString()); 
        } catch (EntityNotFoundException e) {
            txn.rollback();
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .body(new JSONObject().put("Error", "User Doesnt Exist").toString());
        }
    }

//	------------------------------------------------------------------------
//								Private methods

    /**
     * pode se mandar a transacao nos parametros? deletes token of the given user
     *
     * @param email
     */
    private static void deleteToken(String email, Transaction txn) {
        datastore.delete(txn, KeyFactory.createKey("Token", email));
        LOG.info("Token Deleted");
    }

    /**
     * Deletes given user from the database
     *
     * @param email
     */
    private static void deleteUser(String email, Key userKey, Transaction txn) {
        datastore.delete(txn, userKey);
        LOG.info("User Deleted");
    }

    public static ResponseEntity<?> testeQuery() {
        /*
         * JSONObject response = new JSONObject(); EntityQuery.Builder queryBuilder =
         * Query.newEntityQueryBuilder().setKind("Task") .setLimit(12);
         * QueryResults<Entity> tasks = datastore.run(queryBuilder.build()); while
         * (tasks.hasNext()) { Entity task = tasks.next(); // do something with the task
         * } Cursor nextPageCursor = tasks.getCursorAfter(); return
         * ResponseEntity.status(HttpStatus.OK) .body(response.toString());
         *
         */
        return null;
    }
}

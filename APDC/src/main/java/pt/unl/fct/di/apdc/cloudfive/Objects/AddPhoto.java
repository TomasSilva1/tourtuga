package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class AddPhoto {
    public String email, tokenID, routeID, photo;

    public AddPhoto(String email, String tokenID, String routeID, String photo){
        this.email = email;
        this.tokenID = tokenID;
        this.routeID = routeID;
        this.photo = photo;
    }

    public Entity getEntity() {
        Entity photo = new Entity(routeID+"-Photo");
        photo.setProperty("email",email);
        photo.setProperty("photo",this.photo);
        return photo;
    }
}

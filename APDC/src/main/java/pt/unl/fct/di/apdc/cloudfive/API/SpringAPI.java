package pt.unl.fct.di.apdc.cloudfive.API;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.google.appengine.api.datastore.Key;

import pt.unl.fct.di.apdc.cloudfive.Extras.StartThreads;
import pt.unl.fct.di.apdc.cloudfive.Objects.*;

import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
public class SpringAPI {

//	------------------------------------------------------------------------
//									GET

    /**
     * Teste URL
     *
     * @return 200 Ok
     * @throws IOException
     */
    @RequestMapping(value = "/teste", method = RequestMethod.GET,produces = "application/json")
    public ResponseEntity<?> test() {
    	
    	return ResponseEntity.status(HttpStatus.OK).body("yeah you do");
    }

    
    @RequestMapping(value = "/getroutesSearch", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRoutesSearch() {
        return RouteAPISupport.getRoutesSearch();
    }
    
    @RequestMapping(value = "/getroute", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRoute(@RequestParam String routeID) {
        return RouteAPISupport.getRoute(routeID);
    }

    @RequestMapping(value = "/firstmarker", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getFirstMarker() {
        return RouteAPISupport.getFirstMarker();
    }

    @RequestMapping(value = "/getallroutes", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllRoutes(@RequestHeader("offset1") String offset1,@RequestHeader("offset2") String offset2,@RequestHeader("offset3") String offset3,@RequestParam("type") String type) {
        return RouteAPISupport.getAll(offset1,offset2,offset3, type);
    }
    
    @RequestMapping(value = "/getRanking", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRanking(@RequestParam("offset") String offset, @RequestParam("type") String type) {
        return RankAPISupport.rankByType(offset, type);
    }

    @RequestMapping(value = "/getroutecomments", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRouteComments(@RequestParam("routeID") String routeID, @RequestParam("offset") String offset) {
        return RouteAPISupport.getComments(routeID, offset);
    }

    @RequestMapping(value = "/getroutesmall", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRouteSamll(@RequestParam String routeID) {
        return RouteAPISupport.getRouteSmall(routeID);
    }

    @RequestMapping(value="/getstats", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getStats(@RequestParam String date){
        return StatsAPISupport.getStats(date);
    }

    @RequestMapping(value = "/getrecommended", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getRecommended(@RequestParam("lat") double lat, @RequestParam("lg") double lg){
        return RouteAPISupport.getRecommended(lat,lg);
    }

    /**
     * @param request Object with email and token
     * @return 200 Ok if email and token are correct, 406 if user doesnt exist or
     * tokenId is wrong
     */
    @RequestMapping(value = "/getuserinfo", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getUserInfo(@RequestHeader String email, @RequestHeader String tokenID) {
        return UserAPISupport.getUserInfo(new RequestWithToken(email, tokenID));
    }
    
    
    @RequestMapping(value = "/getuserprofile", method = RequestMethod.GET, produces = "application/json", params = "email")
    public ResponseEntity<?> getUserProfile(@RequestParam("email") String email) {
        return UserAPISupport.getUserProfile(email);
    }
    
    @RequestMapping(value = "/getUserLevel", method = RequestMethod.GET, produces = "application/json", params = "email")
    public ResponseEntity<?> getUserLevel(@RequestParam("email") String email) {
        return RankAPISupport.getUserLevel(email);
    }
    
    /**
     * Get users' conversation
     *
     * @param request Object with email and token
     * @return List of user conversations if there are any, or null if none available
     */
    @RequestMapping(value = "/getuserconvs", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getUserConversations(@RequestHeader String email, @RequestHeader String tokenID) {
        return UserAPISupport.getUserConversations(new RequestWithToken(email, tokenID));
    }

    /**
     * Updates token expirationdate
     *
     * @param request Object with email and token
     * @return 200 OK
     */
    @RequestMapping(value = "/validatetoken", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> validateToken(@RequestHeader String email, @RequestHeader String tokenID) {
        return UserAPISupport.validateToken(new RequestWithToken(email, tokenID));
    }
    
    @RequestMapping(value="/getcompletedroutes", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getCompletedRoutes(@RequestHeader String email, @RequestHeader String tokenID){
        return RouteAPISupport.getCompletedRoutes(new RequestWithToken(email, tokenID));
    }
    
    @RequestMapping(value="/getShortNews", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getShortNews(){
        return NewsAPISupport.shortNews();
    }
    
    @RequestMapping(value="/getNew", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getNew(@RequestParam String newID){
        return NewsAPISupport.getNew(newID);
    }

    @RequestMapping(value="/interestedroutes", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getInterestedRoutes (@RequestHeader("email") String email, @RequestHeader("tokenID") String tokenID){
        return RouteAPISupport.getInterestedRoutes(email,tokenID);

    }
//	------------------------------------------------------------------------
//									POST

    /**
     * @param i that wants to Login
     * @return 200 OK
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> logIn(@RequestBody LogIn i) {
        StartThreads.startThread(false,false,false,true);
        return UserAPISupport.logIn(i);
    }


    /**
     * @param r Object with the user Regist Information
     * @return 200 Ok if user doesnt exist, 406 if it does
     */
    @RequestMapping(value = "/registUser", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> register(@RequestBody RegistUser r) {
        StartThreads.startThread(false,true,false,true);
        return UserAPISupport.registUser(r);
    }


    /**
     * Send message to a user creating a notification and saving it to the DB
     *
     * @param msg New Message Object
     * @return 200 OK in case of success or 406 in case of error
     */
    @RequestMapping(value = "/newMsg", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> newMsg(@RequestBody Message msg) {
        return ChatAPISupport.newMsg(msg);

    }

    /**
     * Get chat messages between 2 users
     *
     * @param request New Message Object
     * @return messages
     */
    @RequestMapping(value = "/createchat", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createConversation(@RequestBody Message request) {
        StartThreads.startThread(false,false,true,false);
        return ChatAPISupport.createConversation(request);
    }

    /**
     * Creates a new Route
     *
     * @param request New Route Object
     * @return 200 ok in case of success and 406 in case of error
     */
    @RequestMapping(value = "/createroute", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createRota(@RequestBody Percurso request) {
        StartThreads.startThread(true,false,false,false);
        return RouteAPISupport.createRoute(request);
    }

    @RequestMapping(value = "/wantafriend", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> wantAFriend(@RequestBody WantAFriend request) {
        return RouteAPISupport.wantAFriend(request);
    }

    @RequestMapping(value="/reviewroute",method = RequestMethod.POST, consumes = "application/json",produces = "application/json")
    public ResponseEntity<?> reviewroute(@RequestBody Review request){
        return RouteAPISupport.reviewRoute(request);
    }

//	------------------------------------------------------------------------
//									DELETE

    /**
     * @param r Object with email and token
     * @return 200 Ok if user doesnt exist, 406 if it does
     */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> deleteUser(@RequestBody RequestWithToken r) {
        return UserAPISupport.deleteUser(r);
    }

    /**
     * @param user Object with email and token
     * @return 200 ok if email and password are correct, 406 if not
     */
    @RequestMapping(value = "/logout", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> logout(@RequestBody RequestWithToken user) {
        return UserAPISupport.logout(user.email, user.tokenID);
    }

    @RequestMapping(value = "/blockmatch", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> blockMatch(@RequestBody BlockMatch request) {
        return ChatAPISupport.blockMatch(request);
    }

    @RequestMapping(value = "/removefrommatchlist", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> removeFromMatchList(@RequestBody WantAFriend request){
        return RouteAPISupport.removeFromMatchList(request);
    }

//	------------------------------------------------------------------------
//									PUT	

    @RequestMapping(value="/updateroute", method = RequestMethod.PUT, consumes="application/json", produces="aplication/json")
    public ResponseEntity<?> updateRoute(@RequestBody UpdateRoute request){
        return RouteAPISupport.updateRoute(request);
    }

    @RequestMapping(value="/addphoto", method = RequestMethod.PUT, consumes = "application/json",produces="application/json")
    public ResponseEntity<?> addPhoto(@RequestBody AddPhoto request){
        return RouteAPISupport.addPhoto(request);
    }

    /**
     * @param userReg Object with values to update the user
     * @return 200 Ok if email and token are correct, 406 if user doesnt exist or
     * tokenId is wrong
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateUser(@RequestBody UpdateUser userReg) {
        return UserAPISupport.updateUser(userReg);
    }

    /**
     * Saves/updates user travel preferences
     *
     * @param fav Object with list of favoritie types
     * @return 200 OK or 406 if user doesnt exist or tokenId is wrong
     */
    @RequestMapping(value = "/selectFavTypes", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> selectFavoritTypes(@RequestBody FavoriteTypes fav) {
        return UserAPISupport.updateUserPreferences(fav);
    }

    /**
     * Get chat messages between 2 users
     *
     * @param request New Message Object
     * @return messages
     */
    @RequestMapping(value = "/getchatmessages", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> getChatMessages(@RequestBody Message request) {
        return ChatAPISupport.getChatMessages(request);
    }

    @RequestMapping(value = "/devicetoken", method = RequestMethod.PUT,consumes ="application/json", produces = "application/json")
    public ResponseEntity<?> deviceToken(@RequestBody DeviceTokenUpdate request) {
        return UserAPISupport.setDeviceToken(request.deviceToken, request.email);
    }

    @RequestMapping(value = "/commentroute", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> commentRoute(@RequestBody CommentRoute request) {
        return RouteAPISupport.comment(request);
    }

    @RequestMapping(value = "/completeroute", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> completeRoute(@RequestBody FinishRoute request){
        return RouteAPISupport.finishRoute(request);
    }

    @RequestMapping(value = "/adminUser", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> adminUser(@RequestBody RequestWithToken request){
        return UserAPISupport.adminUser(request);
    }
}
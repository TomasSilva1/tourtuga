package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class WantAFriend {
    public String email, routeID,id,tokenID,name;
    public int day,month,year,hour,minutes;

    public WantAFriend(String email, String routeID, int day, int month, int year, int hour, int minutes,String tokenID, String name){
        this.email = email;
        this.routeID = routeID;
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minutes = minutes;
        this.tokenID = tokenID;
        this.name = name;
        this.id = (email+routeID).hashCode()+"";
    }

    public Entity getEntity(Entity route){
        Entity e = new Entity("Matches",id);
        e.setProperty("email",this.email);
        e.setProperty("routeID",this.routeID);
        e.setProperty("day", this.day);
        e.setProperty("month",this.month);
        e.setProperty("year",this.year);
        e.setProperty("hour",this.hour);
        e.setProperty("minutes",this.minutes);
        e.setProperty("name", this.name);
        route.getProperties().forEach((key,value)->{
            e.setProperty(key,value);
        });
        return e;
    }
}

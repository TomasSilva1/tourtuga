package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

import java.util.Calendar;
import java.util.Date;

public class Message {

	public String email, tokenID, msg, destEmail,offset,hashID;
	public int ano, mes, dia, hora, minutos;
	public long time;

	public Message(String email, String tokenID, String msg, String destEmail, String offset) {
		this.email = email;
		this.tokenID = tokenID;
		this.msg = msg;
		this.destEmail = destEmail;
		Calendar c = Calendar.getInstance();
		this.ano = c.get(Calendar.YEAR);
		this.mes = c.get(Calendar.MONTH);
		this.dia = c.get(Calendar.DAY_OF_MONTH);
		this.hora = c.get(Calendar.HOUR_OF_DAY);
		this.minutos = c.get(Calendar.MINUTE);
		this.offset = offset;
		if (email.compareTo(destEmail) < 0)
			this.hashID = (email + destEmail).hashCode()+"";
		else
			this.hashID = (destEmail + email).hashCode()+"";
		this.time = System.currentTimeMillis();
	}

	public Entity getChatEntity(String name) {
		Entity chat = new Entity("ConvList-"+this.email, this.hashID+"");
		chat.setProperty("dest",this.destEmail);
		chat.setProperty("name",name);
		chat.setProperty("time",this.time);
		chat.setProperty("msg","Now talking to "+name);
		chat.setProperty("time",this.time);
		chat.setProperty("year",this.ano);
		chat.setProperty("month",this.mes);
		chat.setProperty("day",this.dia);
		chat.setProperty("hour",this.hora);
		if(this.minutos < 10)
			chat.setProperty("minute","0"+this.minutos);
		else
			chat.setProperty("minute",this.minutos);
		return chat;
	}
	public Entity getDestEntity(String name){
		Date d = new Date();
		Entity chat = new Entity("ConvList-"+this.destEmail, this.hashID+"");
		chat.setProperty("dest",this.email);
		chat.setProperty("name",name);
		chat.setProperty("time",this.time);
		chat.setProperty("msg","Now talking to "+name);
		chat.setProperty("time",this.time);
		chat.setProperty("year",this.ano);
		chat.setProperty("month",this.mes);
		chat.setProperty("day",this.dia);
		chat.setProperty("hour",this.hora);
		if(this.minutos < 10)
			chat.setProperty("minute","0"+this.minutos);
		else
			chat.setProperty("minute",this.minutos);
		return chat;
	}

	public Entity getMessageEntity(String name){
		Entity chatList = new Entity(this.hashID+"",this.time);
		chatList.setProperty("msg",this.msg);
		chatList.setProperty("user",this.email);
		chatList.setProperty("time",this.time);
		chatList.setProperty("year",this.ano);
		chatList.setProperty("month",this.mes);
		chatList.setProperty("day",this.dia);
		chatList.setProperty("hour",this.hora);
		if(this.minutos < 10)
			chatList.setProperty("minute","0"+this.minutos);
		else
			chatList.setProperty("minute",this.minutos);
		chatList.setProperty("name",name);
		return chatList;
	}
}

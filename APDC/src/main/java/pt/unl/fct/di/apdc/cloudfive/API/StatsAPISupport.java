package pt.unl.fct.di.apdc.cloudfive.API;

import com.google.appengine.api.datastore.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class StatsAPISupport {

    //private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

    public static ResponseEntity<?> getStats(String date){
            Query q = new Query(date);
            List<Entity> it = datastore.prepare(q).asList(FetchOptions.Builder.withDefaults());
            JSONArray arr = new JSONArray();
            JSONObject empty = new JSONObject().put("routes",0).put("matches",0).put("logins",0).put("users",0);
            for(int i = 1;i<32;i++)
                arr.put(empty);
            it.forEach((Entity e) ->{
                arr.put(Integer.parseInt(e.getKey().getName())-1,CommonMethods.fromEntityToJson(e,"day", new JSONObject()));
            });
            return ResponseEntity.status(HttpStatus.OK).body(arr.toString());
    }
}

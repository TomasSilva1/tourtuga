package pt.unl.fct.di.apdc.cloudfive.API;

import com.google.appengine.api.datastore.*;
import org.json.JSONArray;
import org.json.JSONObject;
import pt.unl.fct.di.apdc.cloudfive.Objects.AuthToken;
import pt.unl.fct.di.apdc.cloudfive.Objects.LogIn;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

class CommonMethods {

    private static final Logger LOG = Logger.getLogger(LogIn.class.getName());
    private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    private static final int PAGE_SIZE = 10;

    /**
     * checks if token is in the database and if it hasnt expired, if token is ok
     * then returns true and updates that token expiration data
     *
     * @param tokenID
     * @param email
     * @param txn
     * @param j
     * @return true if token valid
     */
    protected static boolean handleToken(String tokenID, String email, Transaction txn, JSONObject j) {
        try {
            Key tokenKey = KeyFactory.createKey("Token", email);
            Entity token = datastore.get(tokenKey);
            checkForMessages(token, j);
            if (token.getProperty("tokenID").equals(tokenID)
                    && (long) token.getProperty("expirationData") >= System.currentTimeMillis()) {
                token.setProperty("expirationData", System.currentTimeMillis() + AuthToken.EXPIRATION_TIME);
                datastore.put(txn, token);
                LOG.info("Token expiration data updated");
                return true;
            } else {
                LOG.info("Token Deleted");
                return false;
            }

        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    protected static void checkForMessages(Entity token, JSONObject response) {
            if ((boolean)token.getProperty("newMessage"))
                response.put("newMessage", true);
            else
                response.put("newMessage", false);
    }

    /**
     * Constructs JSONObject from Entity
     *
     * @param e
     * @param idString Nome do identificador correspondente à Key
     * @return JSONObject
     */
    protected static JSONObject fromEntityToJson(Entity e, String idString,JSONObject j) {
        j.put(idString, e.getKey().getName());
        for (Map.Entry<String, Object> entry : e.getProperties().entrySet()) {
            if (entry.getKey().equalsIgnoreCase("photo")) {
                j.put(entry.getKey(), entry.getValue());
            } else if (entry.getKey().equalsIgnoreCase("first"))
                j.put("first", new JSONObject().put("lat", ((EmbeddedEntity) entry.getValue()).getProperty("lat")).put("lg", ((EmbeddedEntity) entry.getValue()).getProperty("lg")));
            else
                j.put(entry.getKey(), entry.getValue());
        }
        return j;
    }

    protected static JSONObject queryListWithOffset(Query q, JSONObject response, FetchOptions fetchOptions, Transaction txn, String arrayName,String keyName,String offsetString) {
        QueryResultList<Entity> results;
        PreparedQuery pq = datastore.prepare(q);
        try {
            if(fetchOptions!=null) {
                results = pq.asQueryResultList(fetchOptions);
                response.put(offsetString, results.getCursor().toWebSafeString());
            }else
                results = pq.asQueryResultList(FetchOptions.Builder.withDefaults());
            Iterator<Entity> it = results.iterator();
            JSONArray arr;
            JSONObject j = new JSONObject();
            Entity entity;
            if(!response.has(arrayName))
                arr = new JSONArray();
            else
                arr = response.getJSONArray(arrayName);
            while (it.hasNext()) {
                j = new JSONObject();
                entity = it.next();
                CommonMethods.fromEntityToJson(entity, keyName, j);
                if(arrayName.equalsIgnoreCase("routes"))
                    RouteAPISupport.makeRouteJSON(entity, j);
                arr.put(j);
            }
            response.put(arrayName, arr);
            return response;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            txn.rollback();
            return response.put("Error", "Offset Error");
        }
    }

    protected static JSONObject queryRecommended(Query q, JSONObject response, Transaction txn, String arrayName,String keyName,String offsetString,double lg) {
        QueryResultList<Entity> results;
        PreparedQuery pq = datastore.prepare(q);
        try {
            results = pq.asQueryResultList(FetchOptions.Builder.withDefaults());
            Iterator<Entity> it = results.iterator();
            JSONArray arr;
            JSONObject j;
            int i=0;
            Entity entity;
            if(!response.has(arrayName))
                arr = new JSONArray();
            else
                arr = response.getJSONArray(arrayName);
            while (it.hasNext() && i<10) {
                j = new JSONObject();
                entity = it.next();
                if((double)entity.getProperty("lg")<lg+0.1 && (double)entity.getProperty("lg")>lg-0.1) {
                    CommonMethods.fromEntityToJson(entity, keyName,j);
                    if (arrayName.equalsIgnoreCase("routes"))
                        RouteAPISupport.makeRouteJSON(entity, j);
                    arr.put(j);
                    i++;
                }
            }
            response.put(arrayName, arr);
            response.put(offsetString, results.getCursor().toWebSafeString());
            return response;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            txn.rollback();
            return response.put("Error", "Offset Error");
        }
    }

    protected static FetchOptions setOffset(Transaction txn, String offset){
        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
        if (offset != null && offset != "")
            fetchOptions.startCursor(Cursor.fromWebSafeString(offset + ""));
        return fetchOptions;
    }
}

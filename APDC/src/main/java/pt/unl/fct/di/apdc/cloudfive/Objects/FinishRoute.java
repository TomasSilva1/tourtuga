package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

public class FinishRoute {
    public String tokenID, email, routeID;
    public int distance;
    public boolean finished, isAdmin;

    public FinishRoute(String email, String tokenID, String routeID, boolean finished, int distance, boolean isAdmin){
        this.tokenID = tokenID;
        this.email = email;
        this.routeID = routeID;
        this.finished = finished;
        this.distance = distance;
        this.isAdmin = isAdmin;
    }

    public Entity getEntity(){
        Entity e = new Entity(this.email+"-Done",this.routeID);
        return e;
    }
}

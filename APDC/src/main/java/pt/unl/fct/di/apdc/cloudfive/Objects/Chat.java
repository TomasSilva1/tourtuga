package pt.unl.fct.di.apdc.cloudfive.Objects;

import org.json.JSONObject;

public class Chat {
	public String msg, user;
	public int ano, mes, dia, hora, minutos;

	public Chat(String msg, String user, int ano, int mes, int dia, int hora, int minutos) {
		this.msg = msg;
		this.user = user;
		this.ano = ano;
		this.mes = mes;
		this.dia = dia;
		this.hora = hora;
		this.minutos = minutos;
	}

	public String toJson() {
		JSONObject retValue = new JSONObject();
		retValue.put("msg", this.msg);
		retValue.put("user", this.user);
		retValue.put("ano", this.ano);
		retValue.put("mes", this.mes);
		retValue.put("dia", this.dia);
		retValue.put("hora", this.hora);
		retValue.put("minutos", this.minutos);
		return retValue.toString();
	}
}

package pt.unl.fct.di.apdc.cloudfive.Objects;

import com.google.appengine.api.datastore.Entity;

import java.util.UUID;
public class AuthToken {

	public static final long EXPIRATION_TIME= 1000*60*15;//5 min
	public String email;
	public String tokenID;
	public long creationData;
	public long expirationData;
	
	public AuthToken(String email) {
		this.email= email;
		this.tokenID= UUID.randomUUID().toString();
		this.creationData= System.currentTimeMillis();
		this.expirationData= this.creationData+ AuthToken.EXPIRATION_TIME;
		}
	
	public Entity getEntity() {
		Entity auth = new Entity("Token",this.email);
		auth.setProperty("tokenID", this.tokenID);
		auth.setProperty("creationData", this.creationData);
		auth.setProperty("expirationData", this.expirationData);
		auth.setProperty("newMessage", false);
		return auth;
	}
}

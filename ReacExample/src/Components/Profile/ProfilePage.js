import React, { Component } from 'react'
import $ from 'jquery';
import './ProfilePage.css'
import '../InitialForm/InitialForm.css'
import Nav from '../navigation'
class ProfilePage extends Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            email: '',
            password: '',
            telephone: '',
            profile:'publico',
            cellphone: '',
            address: '',
            photo:'https://scontent.flis7-1.fna.fbcdn.net/v/t1.0-9/16196015_10154888128487744_6901111466535510271_n.png?_nc_cat=103&_nc_ht=scontent.flis7-1.fna&oh=a7c1cf69a87b505dd726e0a588a89bce&oe=5D43E7E9'
        }

        this.onChangeText = this.onChangeText.bind(this);
        this.handleChangesSubmit = this.handleChangesSubmit.bind(this);
        this. handleTextChange = this. handleTextChange.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
    }

    componentDidMount(){
            $.ajax({
                type: "POST",
                url: "https://useful-variety-237416.appspot.com/getUserInfo",
                crossDomain: true,
                data: JSON.stringify({
                    email : localStorage.getItem("email"),
                    tokenID : localStorage.getItem("tokenID")
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",   
                success: (data)=>{
                    if(data.error){
                        localStorage.clear();
                        window.location = ("/");
                    }else{
                        this.setState({
                            username : data.username,
                            email : data.email,
                            password : "Hidden",
                            profile : data.profile,
                            telephone : data.telephone,
                            cellphone : data.cellphone,
                            address : data.address
                        })
                        
                        console.log(data);
                }},
                error: function(){
                    localStorage.clear();
                    window.location = ("/");
                }
            })
        
    }
    onChangeText(e) {
        const target = e.target
        const value = target.value
        const name = target.name
        this.setState({...this.state, [name]: value})
    }

    handleTextChange(e) {
        const target = e.target
        const value = target.value
        const name = target.name
        this.setState({...this.state, [name]: value})
    }

    handleChangesSubmit(e) {
        e.preventDefault()
        $.ajax({
            type: "PUT",
            url: "https://useful-variety-237416.appspot.com/userupdate",
            crossDomain: true,
            data: JSON.stringify({
                oldemail : localStorage.getItem("email"),
                tokenID : localStorage.getItem("tokenID"),
                username : this.state.username,
                email : this.state.email,
                profile : this.state.profile,
                password : this.state.password,
                telephone : this.state.telephone,
                cellphone : this.state.cellphone,
                address : this.state.address
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",   
            success: (data)=>{
                if(data.error){
                    localStorage.clear();
                    window.location = ("/");
                }else{
                    alert("User updated");
                    window.location= ("/profile");
                }
            }
        })
    }
    handleLogOut(e){
        e.preventDefault()
        $.ajax({
            type: "DELETE",
            url: "https://useful-variety-237416.appspot.com/logout",
            crossDomain: true,
            data: JSON.stringify({
                email : localStorage.getItem("email"),
                tokenID : localStorage.getItem("tokenID")
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        })
          
        localStorage.clear();
        alert("Loging Out");
        window.location = ("/");
    }


    render() {
        return(
            <div className="login-page">
            <div className="form">
                <span className="login100-form-title p-b-26">
                    Profile
                </span>
                    <form className="register-form" onSubmit = {this.handleChangesSubmit} >
                        <img className = "profileImage" src={this.state.photo}/>
                        <input type="text" name='username' placeholder = {this.state.username} onChange={this.handleTextChange}/>
                        <input type="password" name='password' placeholder= {this.state.password} onChange={this.handleTextChange}/>
                        <input type="email" name='email'placeholder={this.state.email} onChange={this.handleTextChange}/>
                        <select name ='profile' placeholder="Profile" onChange={this.handleTextChange}>
                            <option value="publico">Público</option>
                            <option value="privado">Privado</option>
                        </select>
                        <input type="number" name='telephone' placeholder={this.state.telephone} onChange={this.handleTextChange}/>
                        <input type="number" name='cellphone'placeholder={this.state.cellphone} onChange={this.handleTextChange}/>
                        <input type="text" name='address'placeholder={this.state.address} onChange={this.handleTextChange}/>
                        <span className="picture">
                        Profile Picture(Optional):
                        </span>
                        <input type="file" name="pic" accept="image/*"></input>
                        
                        <input type="submit" value ="Submit changes" className = "register"></input>
                        <input type="button" onClick={this.handleLogOut} value ="Logout" className = "register"></input>
                    </form>
                    </div>
            </div>
        )
    }
}

export default ProfilePage
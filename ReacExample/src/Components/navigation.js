import React, { Component } from 'react';

class Nav extends Component {
  render() {
    return (
        <nav className="navbar navbar-inverse">
        <div className="container-fluid">
            <div className="navbar-header">
            <a className="navbar-brand" href="#">WebSiteName</a>
            </div>
            <ul className="nav navbar-nav">
            <li><a href="/Profile">PROFILE</a></li>
            <li><a href="/Logout">LOGOUT</a></li>
            </ul>
        </div>
        </nav>
    );
  }
}

export default Nav;
import React, { Component } from 'react'
import $ from 'jquery';
import '../Profile/ProfilePage.css'
import '../InitialForm/InitialForm.css'
class Gbo extends Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            wantedmail:'',
            email: '',
            password: '',
            telephone: '',
            profile:'publico',
            cellphone: '',
            address: '',
            photo:'https://scontent.flis7-1.fna.fbcdn.net/v/t1.0-9/16196015_10154888128487744_6901111466535510271_n.png?_nc_cat=103&_nc_ht=scontent.flis7-1.fna&oh=a7c1cf69a87b505dd726e0a588a89bce&oe=5D43E7E9'
        }

        this. handleTextChange = this. handleTextChange.bind(this);
        this.handleGetUser = this.handleGetUser.bind(this);
    }

    handleTextChange(e) {
        const target = e.target
        const value = target.value
        const name = target.name
        this.setState({...this.state, [name]: value})
    }

    handleGetUser(e){
        $.ajax({
            type: "POST",
            url: "https://useful-variety-237416.appspot.com/getUserInfoH",
            crossDomain: true,
            data: JSON.stringify({
                email : localStorage.getItem("email"),
                tokenID : localStorage.getItem("tokenID"),
                wantedmail : this.state.wantedmail
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",   
            success: (data)=>{
                if(data.error){
                    localStorage.clear();
                    window.location = ("/");
                }else{
                    this.setState({
                        username : data.username,
                        email : data.email,
                        password : "Hidden",
                        profile : data.profile,
                        telephone : data.telephone,
                        cellphone : data.cellphone,
                        address : data.address
                    })
                    
                    console.log(data);
                    alert("User")
                    window.location = ("/gbo");
            }}
        })
    }


    render() {
        return(
            <div className="login-page">
            <div className="form">
                <span className="login100-form-title p-b-26">
                    Profile
                </span>
                    <form className="register-form"  >
                        <span>Insire o utilizador que pretende</span>
                        <input type="text" name='wantedmail' placeholder = {this.state.username} onChange={this.handleTextChange}/>
                        <input type="text" name='username' placeholder = {this.state.username} onChange={this.handleTextChange}/>
                        <input type="password" name='password' placeholder= {this.state.password} onChange={this.handleTextChange}/>
                        <select name ='profile' placeholder="Profile" onChange={this.handleTextChange}>
                            <option value="publico">Público</option>
                            <option value="privado">Privado</option>
                        </select>
                        <input type="number" name='telephone' placeholder={this.state.telephone} onChange={this.handleTextChange}/>
                        <input type="number" name='cellphone'placeholder={this.state.cellphone} onChange={this.handleTextChange}/>
                        <input type="text" name='address'placeholder={this.state.address} onChange={this.handleTextChange}/>
                        <input type="button" onClick={this.handleGetUser} value ="Get User" className = "register"></input>
                    </form>
                    </div>
            </div>
        )
    }
}
    export default Gbo
import React, { Component } from 'react'

import './InitialForm.css'
import $ from 'jquery';

class InitialForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            register: false,
            username: '',
            password: '',
            email: '',
            profile: 'publico',
            telephone:'',
            cellphone:'',
            address:''
        }

        this.showRegister = this.showRegister.bind(this);
        this.hideRegister = this.hideRegister.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
        this.validateRegisterForm = this.validateRegisterForm.bind(this);
        this.validateLogInForm = this.validateLogInForm.bind(this);
    }

    showRegister() {
        this.setState({register: true})
    }

    hideRegister() {
        this.setState({register: false})
    }

    handleTextChange(e) {
        const target = e.target
        const value = target.value
        const name = target.name
        this.setState({...this.state, [name]: value})
    }

    handleLoginSubmit(e) {

        e.preventDefault()
        var tokenID = "879845645616546846";
        if(this.validateLogInForm())
            alert("Preencha todos os campos");
        else{
            $.ajax({
                type: "POST",
                url: "https://useful-variety-237416.appspot.com/logIn",
                crossDomain: true,
                data: JSON.stringify({
                    email : this.state.email,
                    password : this.state.password
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",   
                success: (data)=>{
                    if(data.pwerror)
                        alert("password errada!")
                    else if(data.erro)
                        alert("utilizador inexistente")
                    else if(data.gbo){
                        localStorage.setItem("email", this.state.email);
                        localStorage.setItem("tokenID", data.tokenID);
                        window.location = ("/gbo");
                    }
                    else{
                        localStorage.setItem("email", this.state.email);
                        localStorage.setItem("tokenID", data.tokenID);
                        window.location = ("/profile");
                    }
                }
            })
        }
    }

    validateLogInForm(){
        return !this.state.email || !this.state.password;
    }
    validateRegisterForm(){
        return !this.state.username || !this.state.password || !this.state.email || 
            !this.state.telephone || !this.state.cellphone || !this.state.address;
    }

    handleRegisterSubmit(e) {
        e.preventDefault()
        if(this.validateRegisterForm())
            alert("Preencha todos os campos!");
        else{
            $.ajax({
                type: "POST",
                url: "https://useful-variety-237416.appspot.com/registUser",
                crossDomain: true,
                data: JSON.stringify({
                    username : this.state.username,
                    password : this.state.password,
                    email : this.state.email,
                    profile : this.state.profile,
                    telephone : this.state.telephone,
                    cellphone : this.state.cellphone,
                    address : this.state.address
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",   
                success: (data)=>{
                    console.log(data);
                    localStorage.setItem("email", this.state.email);
                    localStorage.setItem("tokenID", data.tokenID);
                    window.location = ("/profile");
                }
            })
        }
    }

    render() {

        const register = this.state.register

        return(
            <div className="login-page">
                <div className="form">
                    <span className="login100-form-title p-b-26">
                        Welcome
                    </span>
                    {register &&
                        <form className="register-form" onSubmit = {this.handleRegisterSubmit}>
                            <input type="text" name='username' placeholder="Username" onChange={this.handleTextChange}/>
                            <input type="password" name='password' placeholder="Password" onChange={this.handleTextChange}/>
                            <input type="email" name='email'placeholder="Email" onChange={this.handleTextChange}/>
                            <select name ='profile' placeholder="Profile" onChange={this.handleTextChange}>
                                <option value="publico">Público</option>
                                <option value="privado">Privado</option>
                            </select>
                            <input type="number" name='telephone' placeholder="Telephone" onChange={this.handleTextChange}/>
                            <input type="number" name='cellphone'placeholder="Cellphone" onChange={this.handleTextChange}/>
                            <input type="text" name='address'placeholder="Address" onChange={this.handleTextChange}/>
                            <span className="picture">
                                Profile Picture(Optional):
                            </span>
                            <input type="file" name="pic" accept="image/*"></input>
                            
                            <input type="submit" value ="Register" className = "register"></input>
                            <p className="message">
                                Already registered?
                                <a href="#" onClick={this.hideRegister}>Sign In</a>
                            </p>
                        </form>
                    }

                    {!register &&
                        <form className="login-form">
                            <input type="email" name='email' placeholder="email" onChange={this.handleTextChange}/>
                            <input type="password" name='password' placeholder="password" onChange={this.handleTextChange}/>
                            <button onClick={this.handleLoginSubmit}>login</button>
                            <p className="message">
                                Not registered?
                                <a href="#" onClick={this.showRegister}>Create an account</a></p>
                        </form>
                    }
                </div>
            </div>
        )
    }

}

export default InitialForm
import React, { Component } from 'react';
import{BrowserRouter, Route, Switch} from 'react-router-dom'

import InitialForm from "./Components/InitialForm/InitialForm";
import ProfilePage from "./Components/Profile/ProfilePage";
import Gbo from "./Components/GBO/Gbo";

class App extends Component {
  render() {
    return (
      <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path ='/profile' component={ProfilePage}/>
          <Route exact path ='/' component={InitialForm}/>
          <Route exact path ='/Gbo' component={Gbo}/>
        </Switch>
      </BrowserRouter>
      </div>
    );
  }
}

export default App;
